\documentclass{article}
\title{Discussion on STM parameter space}
\author{William Frier}
\date{September 2017}
\usepackage{textcomp} % to use specific charactere

%url link packacge 
\usepackage{hyperref}
\hypersetup{colorlinks = true}

%package to include picture
\usepackage{graphicx}
\graphicspath{ {aux/Images/} }

%referencing
\usepackage[square,sort,comma,numbers]{natbib}

%argmax operator
\usepackage{amsmath}
\DeclareMathOperator*{\argmax}{argmax}

%for intengrals
\usepackage{esint}


\begin{document}
   \maketitle
		
\section{Introduction}
To produce mid-air haptic stimuli with ultrasonic phased array, one needs to modulate the pressure apply to the skin.
Among several modulation techniques, Spatiotemporal Modulation (STM) is the prefered approach to produce mid-air haptic stimuli.

As any modulation techniques, STM relies on a parameter space.
This parameter space allows mainly to define the behaviour of the modulated signal over time.
The parameter space is often put in relation with a perceptual space, or in other words, the perception one will have a of a signal produced with the signal parameter space.
Therefore, the parameter space will aim at definning a set of parameters that affect the perception of the modulation signal it produces.
The advantages of such definitions, is two-fold.
First, it allows to implement or adjust a given stimulus (and its perception) by tweaking a limited set of parameters, as opposed to tweaking the signal directly (i.e. changing the sample values, one at a time).
Second, it allows to share a stimulus with others. 
Hence sharing the parameters value of a given stimulus to others, will assure that the stimulus obtained by others will procuce the same perception.

A parameter space is therefore here to give a simplify representation of the set of all possible stimuli.
To assure simplicity, the parameters within the parameter space should be simple descriptors and their number should be limited.
But ultimately, a good parameter space, is one where the relationship between parameter space and percetual space is simple and accurate.

From this presentation of parameter space, one can already extract two assumptions.
The first one is that the parameteres within the parameters space, do not affect other properties of the modulation signal that are relevant to the perception of the signal.
The second one, is that there is no two distinct modulation signals that could be describe using the same parameters.

In short, the way one defines a parameter space could have huge impact on the perception of the signal design with it, independently from the modulation technique itself.
In this document, I would like to focus on how STM parameter space has been defined so far, and whether another parameter space should be adopted.

\section{Definition}
Before going throught STM parameter space, I would like to remind the reader of the building block of ultrasonic mid-air haptic stimulus, that is the focal point.
Ultrasonic phased arrays focus ultrasound waves on a so-called focal point, which is a point in space, where acoustic pressure is maximised.
If the focal point position coincide with a surface, the acoustic pressure at the focal point, yields to a positive force applied to the surface.
The exact relationship between acoustic pressure and applied force can be described through the acoustic radiation pressure, but this is out of the scope of this document.

However, from this, we can define the focal point as, a point with a position and an amplitude (i.e. the magnitude of the applied force).
Therefore, one could define a focal point $FP$, and its evolution over time $FP(t)$ as $FP(t) = \{p(t), a(t)\}$ , where $p(t) = \{x(t), y(t), z(t)\}$ is the focal point position and $a(t)$ is the focal point amplitude.


\section{Current parameter space}
Following the definition of the focal point, one can define STM as a set of $N$ focal points $FP = \{FP_1, FP_2, \cdots , FP_n\} $, where each focal point is defined with a position and an amplitude, both function of time.
\begin{equation}
FP_i = ( p_i(t), a_i(t)),\ where\ p_i(t) = (x_i(t), y_i(t), z_i(t))
\end{equation}

From this definition, STM is generaly used according to two use cases.
\begin{enumerate}
	\item $p_i(t) = constant$, and $a_i(t) = f_i(t)$, for $i \subseteq [1,N] $
	\item $N = 1$, $p_1(t) = f(t)$, and $a_1(t) = constant$
\end{enumerate}
The first case, as one would have noticed, is similar to AM modulation, where up to $N$ points are used and $f_i(t)$ is the associated modulation signal. 
The second case corresponds to the general application of STM, where a single focal point moves across a predefined curves.
I would like to focus on this second use case for a moment.

In STM, a single focal point ($N = 1$) moves rapidely and repeatidely across a given path, while the intensity is maximal $(a_1(t) = a(t) = 1)$
The function $p_i(t)$ corresponds to parametric curves describing the focal point over time.
Added to the set of position, one will define the rate at which the path is repeated, often refered as the \emph{draw frequency $f_{draw}$ }

Hence, the initial parameter space of STM was defined as the parametric function $p_i(t)$ and the associated draw frequency $f_{draw}$.
However, as stated previously, any given parameter space is only good up to its relation ship with the perceptual space.

Through investigation of STM percetual space, STM parameter space has been challenged \cite{Frier2018}.
In this study, authors added two other parameters to STM parameter space, namely the focal point path length and the focal point speed.
The path length, is the euclidian distance $L_{path}$, the focal point will travel going through one path iteration.
The focal point speed $S_{draw}$ is the averaged speed at which the focal point is moving across the path, and is defined as $S_{draw} = \frac{L_{path}}{f_{draw}}$.
A perceptual study showed the existence of an optimal value for $S_{draw}$, but none for $f_{draw}$.
Hence, the study highlighted that $S_{draw}$ was a better candidate than $f_{draw}$ for STM parameter space.

As any digital signal, STM paths are not continuous but made of a given set of sample position.
Therefore, rather than defining a path, as the parametric curve $p(t)$, it is more correct to determine the STM path as the sorted set of positions $\{p_i\} = \{x_i, y_i, z_i\}$ for $i \subseteq [1, M]$ where $M$ is the number of samples, or in other words, path sampling rate.

In another study from the same authors, investigate the STM perceptual space associated to this new definition of STM path \cite{Frier2019}.
In this second study, authors demonstrated that sampling rate, which represent the cardinal number of the set of position $\{p_i\}$ should be added to the STM parameter space.
However, the study failed to predict what the optimal sampling rate should be.
Nonetheless, it hypothesise the importance of the euclidian distance between two consecutive samples.

These studies challenged STM parameter space.
Sometimes extending it (e.g. adding sampling rate) or changing parameters for others (e.g. changing $f_{draw}$ for $S_{draw}$).
However, it is easy to see that as perceptual studies goes, STM parameter space will get more and more complex.

For example, one could argue that STM path studied are only circles of different sizes, and therefore more complex shapes need to be investigated too.
Additionally, one would have noticed, that the case where $f_i(t) = {p_i(t), a_i(t)}$ has never been investigated either.
One could therefore hypothesise, that such investigations will highlight additionnal constraints on STM parameter space, which will, in turn, increase its complexify even further.
In order to avoid this complexity, one need to look at STM pattern from another point of view.

\section{Definitions}

\subsection{point marker}
Before exploring this new definition, let's define various physical values.
First, let's consider a surface, in the plane $(x,y)$ and with outward normal in the direction $z$.
Then, let's consider an ultrasonic phased array set parallele to this surface.
This array produces focal point, and therefore applies a force $F_{applied}$ on the surface, in the direction $-z$.

$$F_{applied} = (0,0, -F)$$

Rather than considering the whole surface, one could define a marker $m$, which possesses a position $m(t) = (m_x(t), m_y(t), m_z(t)$.
The surface being viscoelastic, it will deform when focal point are created on it, therefore the position will vary over time (i.e. $m(t = t_1) \neq m(t =t_2)$).
This deformation can be express, as displacement $d$, where $d(t) = m(t) - m(0)$.
Displacement can either be normal to the surface $d(t) = (0,0, d_z(t))$, lateral to the surface $d(t) = (d_x(t), d_y(t), 0)$ or a composition of the two $d(t) = (d_x(t), d_y(t), d_z(t))$

One can also define the Fourrier trasform of a time series $A(t)$, and its inverse as $$\hat A(f) = \int A(t) e^{-2 \pi i f t}dt$$ $$A(t) = \int \hat A(f) e^{-2 \pi i f t}df$$

Using this definition of Fourrier transform, one could define $\hat d(f) = (\hat d_x(f), \hat d_y(f), \hat d_z(f))$ the fourrier transform of the displacemnt.
Additionnaly, one could define the ernergy spectral E as $$ E = \int |\hat A(f)|^2 df $$


\subsection{Force Distribution}
It will be incorrect to assume the force applied to the surface to be punctual.
It is known that the force produce by a focal point follow a gaussian distribution.
Hence $$F_{applied}(r | A, \mu, \sigma) = A \cdot e^{- \frac{(r - \mu)^2}{2\sigma^2}}  $$
where r is the distance from the focal point position.

\subsection{propagating waves}

\begin{itemize}
	\item propagating waves
	\item group velocity
	\item group decay
\end{itemize}

\section{Optimising the parameter space}
The current parameter space for STM, is concerned with the focal point set of position $\{p_i\}$, its cardinal number $M$, and the speed at which the focal point moves through the positon $S_{draw}$.
However, the goal of STM and other modulation techniques is not moving the focal point, but rather stimulating the mechanoreceptors embedded under one's skin surface.
Herebelow are three approaches to optimise the parameter space according to their effect on the mechanoreceptors, each are with increasing difficulty.

\subsection{optimising local displacement}
We know that mechanoreceptors are sensitive to displacement.
Therefore to maximise the perception of a STM pattern, one would want to optimise the displacement $d(t)$.
Therefore, one approach will be to place the marker $m$ on the STM path and record the deformation.
A parameter $p$ will be consider optimal if
\begin{equation}
p = p^* = \argmax_p( max( | d(t) | ) )
\label{eq:max_disp}
\end{equation}

\subsection{optimising spectral energy}
However, it is worth noting, that human mechanoreceptors are not sensitive to all frequency equally. 
The litterature have therefore measured a threshold of perception $\hat{th}(f)$.
Where $\hat{th}$ is the minimum displacement one will perceived, if one's skin was indented with a sinusoidal signal of frequency $f$. 
However this value is only known for the z component.

So one could define the spectral energy received by the mechanoreceptor as:

\begin{equation}
E_{mr} = \int | \hat d_z(f) - \hat{th}(f) |^2 df 
\end{equation}

Hence one could consider a parameter p optimal if

\begin{equation}
p = p^* = \argmax_p ( max( E_{mr} ) )
\label{eq:max_freq}
\end{equation}

\subsection{optimising for MR receptive field}
Mechanoreceptors are known to possess a receptive field $RF$ which can roughly be assumed to be circular of radius $r_{mr}$ varying for each mechanoreceptor kind. 
Hence, rather than looking at the displacement $d(t)$ on a point, one could look at the displacement over a surface $S_{RF} = \pi r_{mr}^2$
Equation \ref{eq:max_disp} becomes

\begin{equation}
	p = p^* = \argmax_p ( max ( \oiint_{S_{RF}} d(t)ds ))
\end{equation}


Hower, the fourrier transform over the surface becomes
\begin{equation}
	\hat{D_s}(f) = \int (\oiint_{S_{RF}} d_z(t)ds) dt
\end{equation}

\begin{equation}
	E_{mr} = \int | D_s(f) - \hat{th}(f) |^2 df 
\end{equation}

\begin{equation}
	p = p^* = \argmax_p ( max ( \oiint_{S_{RF}} E_{mr} ds ))
\end{equation}


\section{Parameter Space Candidate}
We saw previously that the parameter space was composed of a set of sample point $\{p_i\}$, its cardinal number $M$, and the focal point speed $S_{draw}$.

\begin{itemize}
	\item $S_{draw} = \frac{dl}{dt}$
	\item dl and dt are related to M
	\item M is bound on the upper limit by $f_{draw}$
	\item $f_{draw}$ is related to $STM_{path}$ and $S_{draw}$
\end{itemize}

loop done, which to consider here?


\subsection{acceleration non-nul}
\begin{itemize}
	\item Speed constant over the circle
	\item What if dl = f(t) ?
	\item what if dt = f(t) ?
	\item what if M = f(t) ?
	\item what if $\{p_i\}$ = f(t)
\end{itemize}
















%\bibliographystyle{natbib}
\bibliography{library}


\end{document}
