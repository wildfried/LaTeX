PS2PDF=ps2pdf
MPOST=mpost
FIGURES=figure1.pdf figure2.pdf
CHAPTERS=chapter1.tex chapter2.tex chapter3.tex 
LTX=pdflatex
BTX=bibtex

all: mapping.pdf

# TEX=pdfetex ?
# Hates running in parallel, adding lock semantics...
%.pdf: %.mp
	@if test -f $@; then :; else \
           trap 'rm -rf metapost.lock' 1 2 13 15; \
           until mkdir metapost.lock 2>/dev/null; do \
              sleep 0.5; \
           done; \
           rm -f $(?:.mp=.mpx) $(?:.mp=.log) $(?:.mp=.1); \
           (cd `dirname $?`; \
           TEX=latex $(MPOST) -jobname=`basename $? .mp` `basename $?`; \
           TEX=latex $(MPOST) -jobname=`basename $? .mp` `basename $?`; \
           $(PS2PDF) -dEPSCrop `basename $(?:.mp=.1)` `basename $@`); \
           rm -f $(?:.mp=.mpx) $(?:.mp=.log) $(?:.mp=.1); \
           result=0; \
           rm -rf metapost.lock; \
        fi

%.pdf: %.tex $(FIGURES) $(CHAPTERS)
	-$(LTX) -shell-escape $(@:.pdf=)
	mv -f $(@:.pdf=.log) $(@:.pdf=.log.bak)
	-while ! cmp -s $(@:.pdf=.log) $(@:.pdf=.log.bak); \
        do \
           $(BTX) $(@:.pdf=); \
           $(LTX) -shell-escape $(@:.pdf=); \
           cp $(@:.pdf=.log) $(@:.pdf=.log.bak); \
        done
	mv -f $(@:.pdf=.log) $(@:.pdf=.log.bak)
	-while ! cmp -s $(@:.pdf=.log) $(@:.pdf=.log.bak); \
        do \
           $(LTX) -shell-escape $(@:.pdf=); \
           cp $(@:.pdf=.log) $(@:.pdf=.log.bak); \
        done
	@-echo \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*
	@-if [ -z `egrep undefined ${@:.pdf=.log}` ]; then \
	   echo "COMPILE SUCCESS - No broken references!"; \
	else \
	   egrep undefined $(@:.pdf=.log); \
        fi
	@-if [ -z `egrep TODO ${@:.pdf=.log}` ]; then \
	   echo "NO TODOS REMAINING - No TODO sections found!"; \
	else \
	   egrep TODO $(@:.pdf=.log); \
        fi
	@-echo \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*
	@cp $@ __$@

.PHONY: clean
clean:
	@rm -f *.aux *.bbl *.blg *.log.bak *.log *.toc *.out *.mpx mpxerr.tex mapping.pdf u7_block_diagram.pdf
	@rm -fr _minted-mapping
