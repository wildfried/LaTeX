\contentsline {chapter}{\numberline {}Acknowledgment}{i}{chapter.1}
\contentsline {chapter}{\numberline {}Summary}{iii}{chapter.2}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}The sense of touch}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}The sense of touch}{7}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Touch is part of the somatosensory system}{8}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Active and Passive touch}{8}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Discriminative and Affective Touch}{10}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Sensation and Perception}{11}{subsection.2.1.4}
\contentsline {section}{\numberline {2.2}Tactile Sensation}{11}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Skin Mechanics}{11}{subsection.2.2.1}
\contentsline {subsubsection}{Skin properties}{12}{section*.6}
\contentsline {subsubsection}{Continuum Mechanics and numerical analysis}{13}{section*.8}
\contentsline {subsection}{\numberline {2.2.2}Mechanotransduction}{13}{subsection.2.2.2}
\contentsline {subsubsection}{Low Threshold Mechanoreceptors}{14}{section*.9}
\contentsline {subsubsection}{Mechanotransduction}{16}{section*.13}
\contentsline {section}{\numberline {2.3}Tactile Perception}{17}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Levels of Perception}{17}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Dimension of Touch}{18}{subsection.2.3.2}
\contentsline {subsubsection}{Roughness}{19}{section*.15}
\contentsline {subsubsection}{Hardness}{19}{section*.16}
\contentsline {subsubsection}{Friction}{20}{section*.17}
\contentsline {chapter}{\numberline {3}Mid-air Tactile Displays and Mid-air Tacile Patterns}{22}{chapter.3}
\contentsline {section}{\numberline {3.1}Mid-air Haptic Display Techonologies}{22}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Air based}{23}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Laser based}{24}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Electric based}{24}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Ultrasound Based}{25}{subsection.3.1.4}
\contentsline {section}{\numberline {3.2}Ultrasonic Phased Array}{26}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Focal Point}{26}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Amplitude Modulation}{28}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Lateral Modulation}{30}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Spatiotemporal Modulation}{31}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Mid-air Tactile Pattern}{32}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Definition}{32}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Mid-air tactile sensation}{34}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Mid-air tactile shape}{35}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}Evaluation}{37}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Acoustic Radiation Pressure}{37}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Skin deformation}{39}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Microneurography}{40}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Perceived Strength}{40}{subsection.3.4.4}
\contentsline {chapter}{\numberline {4}Rendering Speed}{43}{chapter.4}
\contentsline {section}{\numberline {4.1}Theory}{43}{section.4.1}
\contentsline {section}{\numberline {4.2}Vibrometry Study}{45}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Measurement Set-Up}{45}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Preliminary Measurement}{47}{subsection.4.2.2}
\contentsline {subsubsection}{Surface wave propagation speed}{48}{section*.27}
\contentsline {subsubsection}{Frequency response}{48}{section*.29}
\contentsline {subsection}{\numberline {4.2.3}Spatiotemporally Modulated Patterns}{49}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Results}{50}{subsection.4.2.4}
\contentsline {section}{\numberline {4.3}User Study}{51}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Study set-up and procedure}{51}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Results}{52}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Discussion}{53}{section.4.4}
\contentsline {chapter}{\numberline {5}Rendering Update Rate}{56}{chapter.5}
\contentsline {section}{\numberline {5.1}Sampling Strategy}{56}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Pattern Sampling Rate}{56}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Current Sampling Strategies}{58}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Pros and Cons for High Sampling Rate Strategy}{58}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}User Study 1}{59}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Method}{60}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Results}{61}{subsection.5.2.2}
\contentsline {section}{\numberline {5.3}User Study 2}{64}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Method}{64}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Results}{65}{subsection.5.3.2}
\contentsline {section}{\numberline {5.4}Discussion}{66}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}User studies Results}{66}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Haptic Implications}{67}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}Psychophysical Explanation}{69}{subsection.5.4.3}
\contentsline {chapter}{\numberline {6}Spatial Distribution}{71}{chapter.6}
\contentsline {section}{\numberline {6.1}Theory}{72}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Assumptions and limits}{72}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Proposed Approach}{74}{subsection.6.1.2}
\contentsline {section}{\numberline {6.2}Preliminary Work}{75}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Pulse duration}{76}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Pulse envelop}{77}{subsection.6.2.2}
\contentsline {chapter}{\numberline {7}Conclusion}{80}{chapter.7}
\contentsline {section}{\numberline {7.1}Contributions}{80}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Mid-air Haptic Applications}{81}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Haptic Perception}{83}{subsection.7.1.2}
\contentsline {section}{\numberline {7.2}Limitations}{85}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Methodology}{85}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Results}{86}{subsection.7.2.2}
\contentsline {section}{\numberline {7.3}Future work}{88}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}Short Term Work}{88}{subsection.7.3.1}
\contentsline {subsection}{\numberline {7.3.2}Long Term Work}{90}{subsection.7.3.2}
\contentsline {subsection}{\numberline {7.3.3}Complimentary Work}{91}{subsection.7.3.3}
