\section{Theory}
\label{sec:spatial_th}


		\subsection{Assumptions and limits}
		\label{subsec:assumption}

		The work reported in this thesis makes some assumptions to facilitate the investigations on spatiotemporal modulation parameter space.
		Unfortunately, the last study shines light over perceptual effect that the current research approach cannot solve, hence pointing towards the limit of the approach.
		Therefore, the research framework needs to be enlarged and some assumptions made until now, need to be reconsidered.


		%assumption 1: point is infinetesimal != point covers a surface area
		The first assumption is that a focal point is punctual, in the mathematical sense of the term, and therefore the force produced at the focal point, is concentrates at the focal point position.
		Element such as focal point area and pressure distribution within this area, are therefore omitted from the research questions.
		In amplitude modulation, this assumption is correct since the focal point position is constant over time, the skin area stimulated over the interaction is the same, and therefore can be reduced to a single point.
		However, in Spatiotemporal modulation, the point moves overtime, stimulating different area on the skin over time.
		According to the distance between two consecutive positions, it can happen that the area stimulated by the focal point at its first position, overlap with the area stimulated by the focal point at its second position. 
		For example, for a circle of perimeter \SI{8}{\centi\meter} drawn 200 times a second with an ultrasonic phased array driven with \SI{40}{\kilo\hertz} transducer and updated at \SI{16}{\kilo\hertz}, the distance between two consecutive points is \SI{1}{\milli\meter} while the focal point diameter is \SI{8.5}{\milli\meter} (i.e. area of \SI{58}{\square\milli\meter}). 
		In this example, it is likely that 8 consecutive focal point positions will cover the same position on the skin. 
		Meaning that if that position on the skin, represent the position of a mechanoreceptor, the mechanoreceptor will be stimulated 8 consecutive times, while previously it was assumed to be stimulated only once.
		This increased stimulation duration between the two assumptions might be significant, as in the earlier cases, some duration would have been too short to be perceived.
		
		Actually, this argument could be extended further by applying the same thought process to the mechanoreceptors.
		Indeed, mechanoreceptors having a receptive field with a given area, it will be incorrect to consider mechanoreceptor to be punctual.
		Mechanoreceptors receptive field ranges from \SI{101}{\square\milli\meter} for PC mechanoreceptors to \SI{11}{\square\milli\meter} for SAI mechanoreceptors \ref{tab:MR_properties}.
		
		Therefore, the problem is no longer to find how many samples stimulate a given mechanoreceptors location, but how much overlap there is between the focal point stimulation area and the mechanoreceptor receptive field, over time.
		One could therefore hypothesise that the input stimuli is looking more like a ramp up as the focal point approach the mechanoreceptor receptive field centre, and then a ramp down as the focal point goes away from the mechanoreceptor field centre.
		An illustration of this new problematic can be visualised in figure \ref{fig:circle_overlap}.

		However, it is worth noting, that here, an additional assumption is made.
		Indeed, we are assuming that the pressure distribution is constant across the focal point which is against finding from \cite{Hoshi2010, Price2018} and that mechanoreceptors are equality sensitive across their receptive field.
		Therefore, this distribution would need to be taken into account when computing the pressure applied to the mechanoreceptors receptive field.
		
		\begin{figure}
			\centering
			\includegraphics[width=0.9\textwidth]{./Figures/circle_overlap_serie}
			\caption{As the focal point (small circle) moves towards the mechanoreceptor receptive field (big circler), the overlap of the two (blue area) grows (a-d) and then decreases (d-g).
			This may have significance as how strongly the focal point stimulates the mechanoreceptor}
			\label{fig:circle_overlap}
		\end{figure}

		%assumption 2: max inst. displacement != vibration mode 
		Another assumption is that for a periodic stimulus, only stimulus period and peak-to-peak displacement are relevant. 
		The temporal frequency spectrum of displacement is completely ignored, hence omitting contributions from other frequency components to perception.
		Therefore, only the fundamental frequency and its amplitude are compared against the mechanoreceptor frequency sensitivity curve (see section \ref{subsec:mr})/
		Indeed, in Amplitude Modulation, the amplitude signal is often a pure tone therefore, the harmonics amplitude are assumed to be null or small compare to the fundamental frequency. 
		However, in Spatiotemporal Modulation, the stimuli is likely to be a pulse train with a given frequency spectrum and non-null harmonics.
		Hence, considering only the period of repetition of a spatiotemporal pattern is omitting a huge portion of the stimulus information.
		Indeed, harmonics could have a possible effect on the perception, as long as contained in the range of frequency relevant to touch (i.e. up to \SI{500}{\hertz}).
		
		For instance, if one was to assume the haptic stimulation to be equivalent to a perfect pulse train, one could see from equation \ref{eq:pulse_train} that every odd harmonics will be non-null.
		\begin{equation}
			g(t) = w + \sum_{n=1}^{\infty} \frac{2}{n\pi} sin(\pi n w)sin(2\pi\ n \ d_f\  t + \frac{\pi}{2})
			\label{eq:pulse_train}
		\end{equation}
		In this same example, one could hypothesise that a fundamental at \SI{67}{\hertz} might not be perceived since mechanoreceptors threshold is high around this frequency, while its non-null third harmonic at \SI{198}{\hertz} might be perceived since mechanoreceptors threshold is low around this frequency.


		These observations on the previously made assumptions are the building blocks of a new framework of research.
		The next part details further this new framework of research and how it could be used.

		\subsection{Proposed Approach}
		\label{subsec:approach}

		For future work to take into account the observations presented in the previous part, one need to formulate a new research framework.
		This new framework can be divided between two blocks, sensation and perception.
		
		%
		First, the sensation block determines the pressure a focal point applied to a given mechanoreceptor receptive field as it moves along a spatiotemporal pattern.
		The resulting information is a time function of pressure relative to a single mechanoreceptor receptive field, centred on a single position on the palm.
		One could argue that the process needs to be repeated over each location across the spatiotemporal pattern, to record accurately the pressure variations over the spatiotemporal pattern.
		However, Spatiotemporal pattern are periodic signal, for which sample position are resulting from their path being linearly sampled according to a given sampling rate (see chapter \ref{ch:sampling}).
		Hence, a reasonable assumption is that for step smaller than the haptic point width, the computed pressure signal will be equivalent in all position across the pattern path, even though the considered position is not a sample position.
		Step greater than the focal point width are unlikely to be produced, as it will mean some part of the pattern path are not stimulated, and therefore it is likely that the rendered pattern feels discontinuous, which is undesired.

		Then, the perception block compares the deformation resulting from the pressure function, and in particular its frequency content, to the mechanoreceptor frequency sensitivity.
		To do so, one can apply Fast Fourier Transform to the deformation signal and hence obtain the frequency content produced by the mid-air haptic pattern.
		The advantage to analyse the frequency spectrum of the deformation signal rather than the pressure signal, is that mechanoreceptor frequency sensitivity is express relative to \SI{1}{\micro\meter}, making the comparison between the two straightforward.
		Hence, if the magnitude of one of the deformation harmonics is greater than the magnitude of the mechanoreceptor sensitivity at the same frequency, one could predict that the spatiotemporal pattern will be perceived.
		To account for variation in skin properties and perception threshold across users, the difference between magnitudes, might need to be greater that a certain level. 
		This level could potentially be refined through user study.
		Additionally, one could hypothesise that the greater the difference magnitudes, the stronger the pattern will be perceived.
		

		While the sensation block defines a pressure function, the perception blocks processes deformation information.
		Therefore, a third block is required between the two.
		This third block will translate pressure information into deformation information, using the knowledge on skin viscoelastic properties. 
		This step can either be analytical using the various rheological model presented in the section \ref{subsec:skin}
		Alternatively, a Finite Element Model capturing skin deformation under the pressure stimuli of ultrasonic phased array could be developed and implemented.
		Even though this step is unlikely to affect where the harmonics stands on the frequency spectrum, it is known that the skin response is frequency dependent and therefore, some frequency might be attenuated compare to others.

		Once this new framework implemented (Figure \ref{fig:framework}), one could use it to determine whether spatiotemporal patterns can be perceived.
		First, we will need to validate this new framework with simple stimuli, for which detection is already known.
		Once validated, this new framework could be employed to explore more complex mid-air haptic patterns.
		Additionally, a given pattern could be optimised by varying some of its parameters and looking the combinations of parameters that maximise the perceived strength.
			
		%approach already used in electrostatic display!
		It is worth mentioning that this approach is not the first of his kind.
		For instance, similar framework have been successfully implemented for studying electrostatic display (\cite{Vardar2017a}).

		\begin{figure}
			\centering
			\includegraphics[width=0.9\textwidth]{./Figures/framework}
			\caption{Proposed Research Framework. Characterising the pressure applied at the mechanoreceptor receptive field could help to predict whether a given mid-air haptic stimulus will be perceived.}
			\label{fig:framework}
		\end{figure}

