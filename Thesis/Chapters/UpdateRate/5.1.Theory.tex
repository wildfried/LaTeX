\section{Sampling Strategy}
\label{sec:sampling}

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\textwidth]{./Figures/banner_small}
	\caption{(a) A mid-air tactile pattern such as a circle is sampled into a set of successive positions, here 10. 
		(b) Each sample point is presented during a given interval of time proportional to the total number of sample points. 
		(c) Increasing the number of sample points will increase the rendering  delity, but will also decrease the stimulation duration of each sample point.}
	\label{fig:teaser}
\end{figure}

With STM, a tactile pattern is produced using a single mid-air tactile point.
Therefore, producing the pattern shape as well as its associated sensation requires rapid and repeated updates of the point properties.
In this part, we discuss the update rate requirements and its relation to what we call the pattern sampling rate.
Then, we present the current sampling strategy applied in the literature.
Finally, we evaluate the current strategies in an effort to find a new, more optimal sampling strategy.

\subsection{Pattern Sampling Rate}
\label{subsec:sampling_pattern}
%current refresh rate and what they are used for
We saw in section \ref{sec:pattern} that a tactile pattern was composed of a shape and a sensation.
However, tracing a pattern shape requires sampling the shape into a set of intermediate discrete positions, referred to as sample points (see Figure~\ref{fig:teaser}.a).
When rendering the pattern shape, the tactile point position will thus be updated successively to each of those sample points position.
The number of sample points, referred to as \emph{sampling rate}, can vary.
However, the greater the sampling rate, the smoother the pattern representation.
For instance a circle traced with 20 points will appear smoother than a circle traced with 10 points (see Figure~\ref{fig:teaser}-b\&c).

With Amplitude Modulation, adjusting the tactile sensation is equivalent to tuning its modulation frequency \cite{Obrist2013}.
The modulation frequency being the rate at which a specific skin patch is repeatedly stimulated, in the case of STM the modulation frequency is similar to the rate at which the pattern is drawn, referred to as \emph{draw frequency}
Indeed, drawing a circle at \SI{5}{\hertz}, means tracing out the circle path 5 times per second and therefore stimulated each position along the circle 5 times per second. 
Therefore, adjusting the tactile sensation is equivalent to tuning its draw frequency.

Furthermore, if the circle sampling rate is of 10 points per circle, it means that the point will move through each 10 positions 5 times a second.
In other words, the point position will be updated 50 times per second ($\SI{5}{\hertz} \times 10$ points).
Thus, a mid-air tactile display, updating a given pattern, needs an \emph{update rate} equivalent to the product of the pattern sampling rate times the pattern modulation frequency (see equation~\ref{equation:sampling_rate}).
%
\begin{equation}
	\textrm{update rate} = \textrm{sampling rate} \times \textrm{draw frequency}
    \label{equation:sampling_rate}
\end{equation}
%
We would like to highlight that the update rate thus defined is not necessarily the maximum update rate that a mid-air tactile display can achieve.
Therefore, for update rates lower than that achievable by the hardware, it is possible to increase either the pattern sampling rate or draw frequency without decreasing the other.
However, at update rates close to hardware maximum capabilities, a trade-off between sampling rate and draw frequency is required.

\subsection{Current Sampling Strategies}
\label{subsec:sampling_strat}
We define as sampling strategy the tuning of pattern sampling rates according to specific criteria.
We found that all previous work use the same strategy that is to determine the pattern sampling rate as the maximum update rate the hardware can achieve, divided by the draw frequency.
For instance, \citet{Kappus2018} are producing a circle at \SI{200}{\hertz} using the full \SI{20}{\kilo\hertz} update rate achievable by the hardware used in their experiments.
We believe the reason is that the researchers assume that the greater the update rate the better. 
This assumption seems pertinent as it is a strategy already being used for different modalities. 
For instance, in vision and audio a higher sampling rate will reduce artefacts such as motion blur and flickering, for vision, and aliasing, for audio.
We referred to this strategy as the \emph{high sampling rate strategy}.

\subsection{Pros and Cons for High Sampling Rate Strategy}
\label{subsec:sampling_high}
%Pro: spatial resolution 
A high sampling rate strategy can support high draw frequencies without distorting the tactile pattern shape.
For example, a simple pattern shape such as a \SI{150}{\milli\metre} circumference circle can be rendered at \SI{500}{\hertz} (the upper limit of vibrotactile frequency relevant to touch) using 40 sample points spaced out every \SI{3.75}{\milli\metre}.
The spacing between two consecutive points being less than the tactile point radius ($\approx \SI{4.3}{\milli\metre}$), one would expect that the pattern will still be perceived as smooth and continuous.

%cons 1: stimulation duration too short
However, the problem is that a high sampling rate strategy results in shorter stimulation durations.
Indeed, increasing the pattern resolution decreases the relative stimulation duration of each point.
For example, a device running at a update rate of \SI{20}{\kilo\hertz}, this means that the stimulation duration of one sample is \SI{50}{\micro\second}.
This duration is 2 to 3 order of magnitude lower than the perceptible sense of touch temporal resolution found by \citet{Loomis1981} that was estimated to be between \SI{2}{\milli\second} and \SI{40}{\milli\second}.
Moreover, this draw frequency is 2 orders of magnitude higher than the vibrotactile frequency range relevant to our mechanoreceptors.
It is therefore unlikely that our skin can perceived such rapid tactile stimuli.

%cons 2: inconsistency across pattern...
Another challenge of using a higher sampling rate strategy is the possible inconsistency in sampling rate between tactile patterns being traced with different draw frequency.


%transition
%What should be an appropriate sampling strategy?
The discussion thus far has therefore revealed that pattern variability demands customised pattern sampling rates, otherwise inherent variations will manifest themselves in an uncontrolled and misunderstood tactile perception.
This chapter is addressing the above mentioned challenges and propose mitigation strategies.

