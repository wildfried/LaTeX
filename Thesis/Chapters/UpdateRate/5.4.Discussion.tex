\section{Discussion}
\label{sec:sampling_disc}
In the current paper, we investigated a sampling strategy that maximised the pattern perceived strength. 
Using circular patterns rendered with different amounts of sampling points, we established a relationship between pattern sampling rate and pattern perceived strength.
After discussing the user studies results, we will try to explain those same results using the psychophysical literature on the perception of touch. 
Finally, we will cover the implication of our work for tactile feedback designers.

\subsection{User studies Results}
\label{subsec:disc_us}
%effect on low draw frequency 
In the two user studies, we demonstrated that pattern sampling rate has an effect on pattern perceived strength.
However, significant effect was limited to patterns with draw frequencies ranging from \SI{2}{\hertz} to \SI{10}{\hertz}.
	%new
Although variability can be observed in user results magnitude, which could be accounted for user's subjective judgement, the overall trends are common across participants and can be modelled.
	%end new
%Quadratic Behaviour
Using a regression model, we successfully fitted the pattern perceived strength to a quadratic function of the logarithm of the sampling rate (see equation~\ref{equation:model}).

%optimum resolution --> invariant point inter-distance!?!
From these regression functions, we identified an optimal sampling rate for patterns rendered at \SI{10}{\hertz}, of 10.77, 15.01 and 20.30 points for circumferences \SI{100}{\milli\metre}, \SI{150}{\milli\metre} and \SI{200}{\milli\metre} respectively.
By taking the ratio of the pattern circumference over the optimal sampling rates, we obtained an optimal distance between sample points of $\SI{9.7}{\milli\meter}\pm0.3$
The low variation between optimal distances between sample points, designate this distance as an invariant for maximising pattern perceived strength across pattern sizes.
We found similar results with draw frequency of \SI{2}{\hertz}, for which the optimal distance between samples points was in average equal to $\SI{6.5}{\milli\metre}\pm0.8$. 
However, the optimal distances obtained are different across pattern draw frequency and despite our effort, we could not establish a clear relationship between optimal distance and draw frequency.

%threshold resolution --> invariant?
Using the user study results, we also found that perceived strength plateaus when the pattern sampling rate is greater than a given threshold. 
This threshold is in average $245\pm6.2$ points and $89\pm3.3$ points for patterns at \SI{2}{\hertz} and \SI{10}{\hertz}, respectively.
The low variation between threshold averages suggests the sampling rate threshold to be invariant across pattern sizes, although we could not establish the relationship between threshold and pattern draw frequency.

%warning about high frequency
Even though our study showed no effect of sampling rate on perceived strength for patterns at high frequency, we would like to point out that, when observed, the effect occurs only for sampling rate under 200 points.
However, high frequency patterns can not currently be rendered with sampling rates up to 200 points.
For instance, the mid-air tactile display we used could render a pattern at \SI{80}{\hertz} with only 24 points at most.
It is likely that technology will improves and allow to render high frequency patterns with sampling rate of 200 points or more. 
Until then, we cannot completely ruled out the effect of sampling rate on perceived strength in the case of high frequency patterns.

%change of strength
Finally, on Figure~\ref{fig:plots2}, one may note that the maximum perceived strength vary with the pattern draw frequencies and sizes.
However, this can be explained with the work presented in chapter \ref{ch:speed}, which shows that perceived strength varies with the tactile point speed (i.e. draw frequency times pattern circumference).

\subsection{Haptic Implications}
\label{subsec:disc_imp}
Higher sampling rate does not always improve tactile perception and quite often, the old cliche is true: less is more.
Such design insights can be hugely beneficial to haptic engineers, developers and designers.
Using the general trend found in the user-study results, we therefore propose ways and relationships for such parameters and variations to be hidden behind easy-to-use software packages.

%unlock new sensations - once unlock sensation can be scaled
First, we encourage tactile feedback designers working with mid-air tactile display to decrease sampling rate whenever rendering tactile pattern with low frequency.
Decreasing sampling rate for a pattern that initially cannot be perceived, might suddenly unlock the said pattern.
For instance, circular patterns as studied here, could not be perceived below \SI{20}{\hertz} with a high sampling rate.
However, when the sampling rate was lowered, the same circular pattern could be perceived as low as \SI{2}{\hertz}.
%range double
We would like to emphasise that since tactile perception of frequency follows a Weber-law, the range 2-\SI{10}{\hertz} is half as wide as the range 10-\SI{200}{\hertz}.
Hence increasing by 50\% the range of discriminable frequency one could now apply to mid-air tactile patterns.
%threshold variation
We also would like to remind our readers, that in our study, we consider low frequency any frequency less than or equal to \SI{10}{\hertz}.
However our study focusing only on circular patterns, the \SI{10}{\hertz} frequency threshold might vary for other pattern shapes, and hence ask the reader to interpret the values of this study carefully when applied to different pattern shapes.

%sensation perceived strength can be maximised + maximum can be scaled
Then, we would like to invite feedback designers to adjust the sampling rate of a given mid-air tactile pattern, whenever it is possible, in order to maximise its perceived strength.
We also remind designers that this optimal sampling rate is proportional to the pattern size.
Hence, when scaling a given pattern, the sampling rate should be scaled accordingly.

%difference drawing shape - complete shape
As no previous work exploring adjusting sampling strategy has been undertaken, we expect the possibility to render low frequency pattern to be unveiled for most designers working with mid-air tactile display.
Moreover, low frequency patterns, operating at much lower speed than usual pattern rendered with STM, are now expected to be perceived as moving points rather than complete shapes.
Moving points, providing richer information (such as start $\&$ end locations, direction of motion and rate of movement, all of which are masked at higher speeds), are better recognised than multi-points pattern.
This has already been demonstrated for contact devices, which used unistroke patterns (\cite{Ion2015}). 
We believe the distinction to hold between low and high frequency mid-air tactile patterns too.
Hence, thanks to the sampling strategy we presented, a new horizon of possibilities has been made available to the designers. 
We encourage designers to experiment with and investigate those new possibilities.

%integrate into a tool so user don't have to bother.
Finally, we are conscious all the results presented here could be overwhelming for tactile feedback designers. 
However, the invariants identified in the second user study should ease the implementation of our results into design tools as hidden parameters.
Thus improved design tools will allow current tactile feedback designers to stay oblivious to sampling strategies.

\subsection{Psychophysical Explanation}
\label{subsec:disc_psy}
In an attempt to further understand the results reported in this study, we discuss here some hypotheses related to the psychophysics of the sense of touch.

%frequency perception curve
Firstly recall that, for AM, different modulation frequencies are perceived with different strength, even though the amplitude of the stimulation remains the same, \SI{200}{\hertz} being the frequency perceived the strongest (\cite{Gescheider2004}). 

%non-sine -->use fft
However, STM stimulation can no longer be described as a sinusoid like for AM and LM, but more like a pulse train with alternation between intervals of stimulation and non-stimulation. 
Using a Fourier expansion, this pulse train can be decomposed as a sum of sinusoidal signals, thus unveiling the presence of harmonics that are higher in frequency, with an amplitude depending on the pulse width.
Decreasing the sampling rate may inadvertently increase the harmonic's amplitude close to \SI{200}{\hertz}, and thereby increase the associated perceived strength.

%viscoelastic --> skin relax
Another hypothesis is related to the skin viscoelastic properties (see section \ref{subsec:skin}).
High sampling rate stimulation leads to stimulation durations being too short for the skin deformation to reach the required mechanoreceptors depth. 
At first, this hypothesis might seem unlikely since higher frequency patterns yield to tactile perception nonetheless. 
However by definition the rate at which the stimulation is repeated at a single location is much faster for high draw frequencies than for low draw frequencies.
Therefore, it is plausible that at high frequencies the skin indentation builds up as the pattern is repeated over and over again whereas at low frequencies the elastic skin relaxes entirely between stimulation intervals.

%MR involved
Until now, mid-air haptics was relying on stimulating RA and PC mechanoreceptors that are sensitive to vibrations higher in frequency than the one involved in this study (\cite{Johansson2009}). 
However, one could note that as the tactile points moves across the skin surface, different groups of SA1 mechanoreceptors might be stimulated.
Indeed, SA1 mechanoreceptors are mostly sensitive to the stimulus onset and offset (i.e. transient stimulus).
Therefore, as the mid-air stimulus moves from one position to another, the stimulus is offset at the old position and onset at the new position.
However, when a sampling rate is too high the sample position difference is lower than SA1 receptive field (\cite{Vallbo1984}), and do not lead to this transient behaviour and therefore to tactile perception.

%transition mr model
Ultimately, using a mechanotransduction model as the one presented by \citet{Saal2017}, one could test some of these hypotheses.
Although, such models only predict stimulus detection, but will not determine optimal stimulation.

