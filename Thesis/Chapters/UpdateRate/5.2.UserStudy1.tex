\section{User Study 1}
\label{sec:sampling_us1}

%set up picture
\begin{figure}
	\centering
    \includegraphics[width=0.6\textwidth]{Figures/user}
    \caption{The set-up for the user studies. Participants were perceiving the mid-air tactile pattern on their left palm while rating each pattern on a designated laptop.}
    \label{fig:setup}
\end{figure}

%results plots for the first user study
\begin{figure*}
  \centering
  \includegraphics[width=\textwidth]{Figures/plots1}
  %\caption{The standardise perceived strength as a function of the number of sampling points, for different frequencies. Light curves represent participants responses and bold curves represent responses average. In each case the pattern was a \SI{150}{\milli\metre} circumference circle}
  \caption{The standardise perceived strength as a function of the number of sampling points, for a  \SI{150}{\milli\metre} circumference circle rendered at different frequencies. Light and bold curves represent participants responses and responses average, respectively.}
  \label{fig:plots1}
\end{figure*}

There are various modulation methods and sampling strategies that can produce a mid-air tactile pattern using focused ultrasound.
These methods and strategies predominantly depend on the available hardware being used.
There has not been however any discussion on how sampling strategy affects the overall pattern perception.
This section describes how we undertook to investigate the relation between sampling strategy and pattern perception.
In particular, we focus on the pattern perceived strength relative to the pattern sampling rate.

\subsection{Method}
\label{subsec:us1_meth}
%hypothesis:
Our hypothesis was that the pattern sampling rate will have an effect on the perceived strength.
%Name of the test
To test this, we ran a magnitude estimation task \cite{Jones2013}.
In this task, participants had to estimate the perceived strength for patterns rendered with different sampling rates.

%setup
In total 26 participants took part in the user study (6 females, average age$\pm$SD: 29.3 $\pm$5.2).
Participants were sitting comfortably on an office chair, which they were free to adjust to their liking.
On the left of the participant, there was an acrylic box, roughly at their hip level.
The box was \SI{200}{\milli\metre} high and a mid-air tactile display UHEV1 from Ultrahaptics Ltd. was lying at the bottom of the box. 
An aperture was cut on the top box, so participant can rest their left hand over it while experiencing the different mid-air tactile patterns.
Before starting the study, an initial focal point was presented to the user hand, so they can align their palm with the array output.
To avoid participants responses to be biased by surrounding noises, participants were wearing noise cancelling headphones which were playing pink noise.
On the desk, in front of the participants, a laptop was running the experimental protocol. 
Participants could read instructions from the laptop screen and input their strength estimates via a computer mouse.
Figure~\ref{fig:setup} shows the overall set-up.

%which pattern & Why
To test our hypothesis we used a set of patterns with various sampling rates. 
To avoid shape related effects, all of these were variations on a circular pattern.
All patterns were a \SI{150}{\milli\metre} circumference circle (i.e. $\approx$ \SI{24}{\milli\metre} centimetre radius), as it covers most of the palm of the participant (human palm width mostly varies between \SI{75}{\milli\metre} and \SI{95}{\milli\metre} (\cite{Komandur2009}) ).
Circles have also a clear periodic property and its intermediately positioned points can be easily made equally spaced, all of these limiting possible artefacts due to shape geometry.
In this study, we also wanted to test whether the sampling rate of the pattern will affect the sensations of different patterns equally.
Therefore we picked 6 different draw frequencies for the presentation of the pattern, as to cover different octaves and the sensitivity ranges of different mechanoreceptors (\cite{Johansson2009}).
An illustration of such circular pattern is depicted in Figure~\ref{fig:teaser}-a, while Figure~\ref{fig:teaser}-b\&c show how the sampling rate affect the pattern spatiotemporal properties.
The range of possible samples rates varied with the draw frequency.
Due to this, we picked a total of 6 to 11 pattern sampling rates, depending on the draw frequency, which accounted for a total of 51 distinct patterns.
Each pattern was repeated 3 times, making for a total of 153 stimuli in the study.

%Procedure
Each mid-air tactile pattern was presented to the participants left palm for 3 seconds.
At the end of the stimulus, a numeric pad was displayed on the screen as well as an instruction inviting participants to enter their perceived strength estimates.
Prior to the user study, participants were instructed to estimate the pattern perceived strength using their own scale.
Additionally, participant were asked to rate the perceived strength from 0 as the minimum (i.e.\ did not feel the pattern), to infinite, using whole numbers (i.e.\ no decimal) and to be as consistent as possible in their estimation throughout the study. 
Finally, participants were reminded to focus only on the pattern perceived strength and to omit any other qualitative evaluation from their rating (e.g., smoothness or simultaneousness).
After participants validated their response, the next pattern was presented after a two seconds break until participants rated all stimuli.
The patterns order were presented in a randomised order.
The whole study lasted about 20 minutes.

\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{Figures/Fitting4}
    \caption{Data post-processing steps. (a) Raw data, (b) Standardised data, (c) Significant data, and (d) Fitted model.}
    \label{fig:fit}
\end{figure}

%result quadratic model
\begin{table}
  \centering
  \begin{tabular}{l r r r}
    % \toprule
    & \multicolumn{3}{c}{\small{\textbf{Quadratic Mixed Model}}} \\
    \cmidrule(r){2-4}
    {\small\textit{Rate}}
    & {\small \textit{$R^2$}}
    & {\small \textit{N. opt.}}
    & {\small \textit{N. lim.}} \\
    \midrule
    
    \SI{2}{\hertz} & 0.68 & 22.4 & 236.6  \\
    \SI{5}{\hertz} & 0.72 & 17.5 & 119.2 \\
    \SI{10}{\hertz} & 0.62 & 15.8 & 149.4 \\
    % \bottomrule
  \end{tabular}
  \caption{Quadratic mixed model results for frequency \SI{2}{\hertz}, \SI{5}{\hertz} and \SI{10}{\hertz}. Results include $R^2$, optimal sampling rate and  sampling rate limit.}~\label{tab:Quad1}
\end{table}

\subsection{Results}
\label{subsec:us1_res}
As participants were using different scale to estimate the pattern perceived strength, we first standardised the participants responses.
That is, we divided each participant estimate by their highest response (\cite{Jones2013,Strohmeier2017}).
As we were interested in studying each pattern sensation separately, we further separated the data into 6 subsets, one for each pattern draw frequency.
Post-processed participants perceived strength estimates are shown in Figure~\ref{fig:plots1} as a function of pattern sampling rate. 
We invite the reader to note that the $x$-axis of the figure is logarithmically scaled as the pattern sampling rates spread across 4 orders of magnitude.

%step 0 - saphiro-wilk to test for normality
Each data subset was found to be unlikely to follow a normal distribution (Shapiro-Wilk, $p < .05$).
%Step 1 friedman 
Therefore we ran a Friedman test on each data subset to test whether the perceived strength ratings were significantly different across sampling rate values.
The Friedman test indicated significant differences between sampling rates groups for each draw frequency: \SI{2}{\hertz}$(\chi^2(10) = 199.1, p < .001)$,  \SI{5}{\hertz}$(\chi^2(9) = 179.4, p < .001)$, \SI{10}{\hertz}$(\chi^2(8) = 109.9, p < .001)$, \SI{20}{\hertz}$(\chi^2(7) = 43.4, p < .001)$, \SI{40}{\hertz}$(\chi^2(6) = 45.6, p < .001)$ and \SI{80}{\hertz}$(\chi^2(5) = 15.26, p = .009)$.  

%Step 2 wilcoxon
To further determine whether the differences were significant across the whole range of sampling patterns, we ran a pairwise Wilcoxon signed-rank test, with Bonferroni correction to avoid type 1 error.
For draw frequencies \SI{20}{\hertz}, \SI{40}{\hertz} and \SI{80}{\hertz}, the Wilcoxon test indicated significant differences only between 1 or 2 pairs of sampling rates.
We therefore discarded these draw frequencies for the end of the data analysis.
However, for draw frequencies of \SI{2}{\hertz}, \SI{5}{\hertz} and \SI{10}{\hertz}, the Wilcoxon test indicated significant differences for all sampling rate pairs, as long as the sampling rate was lower than 200, 96 and 48 points, respectively.


%Step 3 Quadratic mixed model
The fact that the upper sampling rate interval leads to no significant differences, suggest that no specific behaviour could be extracted from that part of the data.
Furthermore, the fact the corresponding perceived strength plateau around 0, suggest that the participants did not perceive these patterns.
Those two points, motivated us to discard the data for the next step of the analysis and focus on the lower sampling rate interval.

On the remaining data, which correspond to the left part of the curve on Figure~\ref{fig:plots1}, the reader can see that, the pattern perceived strength seems to follow a quadratic behaviour.
This apparent quadratic behaviour motivated us to use a quadratic linear model, to fit our data. 
The model we used for regression can be seen in equation~\ref{equation:model}
%
\begin{equation}
	\textrm{strength} = a\log_{10}^2(\textrm{sampling}) + b\log_{10}(\textrm{sampling}) + c
	\label{equation:model}
\end{equation}
%
We remind the reader, that the model uses logarithmic values as the plots on Figure~\ref{fig:plots1}, where the quadratic behaviour can be observed, are using logarithmic x-axes.
The model gave $R^2$ values of 0.68, 0.72 and 0.63 for the pattern draw frequency \SI{2}{\hertz}, \SI{5}{\hertz} and \SI{10}{\hertz} respectively.

%Step 4 Compute maximum
An $R^2$ value greater than 0.6 is considered high.
We can therefore conclude that the quadratic model is a good fit to model our data.
Hence we used the coefficient from the model to estimate the pattern sampling rate that was giving the highest perceived strength.
We found that the optimal pattern sampling rate was 22.4, 17.5 and 15.8 points for draw frequency \SI{2}{\hertz}, \SI{5}{\hertz} and \SI{10}{\hertz}, respectively.
Finally we estimated the sampling rate threshold that was leading to the pattern to be perceived or not.
We found a threshold of 236.6, 119.2 and 149.4 points for pattern draw frequency \SI{2}{\hertz}, \SI{5}{\hertz} and \SI{10}{\hertz}, respectively.
The post-processing step can be visualised in Figure~\ref{fig:fit} and the results of the data fitting are summarized in Table~\ref{tab:Quad1}.
