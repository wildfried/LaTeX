\section{User Study 2}
\label{sec:sampling_us2}

\begin{figure*}
  \centering
  \includegraphics[width=\textwidth]{Figures/plots2}
  \caption{The standardised perceived strength as a function of the number of sampling points, for different frequencies and circle circumferences. Light curves represent participant responses and bold curves represent responses average.}
  \label{fig:plots2}
\end{figure*}

%size variation and not stimulation time variation. Stimulation time variation means that the "overall" stimulation time will decrease...
In the first user study, we were able to determine a relation between pattern perceived strength and sampling rate. 
However this relation parameters are varying with the pattern draw frequency.
In this second user study, we aim to determine whether these relation parameters vary as well when the pattern size changes.

\subsection{Method}
\label{subsec:us2_meth}
We use the same protocol and set-up as in the first study.
The new stimuli set was composed of 2 draw frequencies.
We chose \SI{2}{\hertz} and \SI{10}{\hertz}, as they are the two boundary frequencies for which the effect of sampling strategy was observed in the first user study.
There were 11 and 9 pattern sampling rates for the two draw frequencies, \SI{2}{\hertz} and \SI{10}{\hertz}, respectively.
We used 3 different pattern sizes, which were 100, 150 and \SI{200}{\milli\metre} circumference. 
There was a total of 60 distinct patterns. 
Each pattern was repeated 3 times, making a total of 180 stimuli.
The user study lasted about 25 minutes.
In total 26 participants (4 females, average age$\pm$SD: 30$\pm$5.9) took part in the study. 

\begin{table}
  \centering
  \begin{tabular}{l l r r r}
    % \toprule
    & & \multicolumn{3}{c}{\small{\textbf{Quadratic Mixed Model}}} \\
    \cmidrule(r){3-5}
    {\small\textit{Rate}}
    & {\small \textit{Circum.}}
    & {\small \textit{$R^2$}}
    & {\small \textit{N. optimal}}
    & {\small \textit{N. limit}} \\
    \midrule
    \SI{2}{\hertz} & 0.10m & 0.69  & 18.72 & 240.55  \\
    \SI{2}{\hertz} & 0.15m & 0.60  & 20.95 & 241.76 \\
    \SI{2}{\hertz} & 0.20m & 0.61  & 27.95 & 260.29\\
    \SI{10}{\hertz} & 0.10m & 0.67  & 10.77 & 92.09 \\
    \SI{10}{\hertz} & 0.15m & 0.60  & 15.01 & 90.54 \\
    \SI{10}{\hertz} & 0.20m & 0.60  & 20.30 & 84.52\\
    % \bottomrule
  \end{tabular}
  \caption{Quadratic mixed model results for frequency \SI{2}{\hertz} and \SI{10}{\hertz} across the different pattern sizes. Results include $R^2$, optimal sampling rate and  sampling rate limit}~\label{tab:Quad2}
\end{table}

\subsection{Results}
\label{subsec:us2_res}
The data collected were standardised as in the first user study. 
We also separated the standardised responses into 6 subsets according to pattern draw frequency and pattern size.
Figure ~\ref{fig:plots2} shows the resulting rating after standardisation for each data subset. 

%step 0 - saphiro-wilk to test for normality
Each data subset was likely not normally distributed (Shapiro-Wilk, $p < .05$).

%Step 1 friedman 
Therefore, we ran a Friedman test on each data set to test whether the perceived strength rating were significantly different across the corresponding number of sampling rate.
For patterns at \SI{2}{\hertz}, Friedman test indicated significant differences as $\chi^2(10)=200.5, p<.001$, $\chi^2(10)=212.3, p<.001$ and $\chi^2(10)=208.9, p<.001$, for circumferences \SI{100}{\milli\metre}, \SI{150}{\milli\metre} and \SI{200}{\milli\metre}, respectively.
For pattern at \SI{10}{\hertz}, Friedman test indicated significant differences as $\chi^2(8)=138.1, p<.001$, $\chi^2(8)=111.0, p<.001$ and $\chi^2(10)=84.0, p<.001$, for circumferences \SI{100}{\milli\metre}, \SI{150}{\milli\metre} and \SI{200}{\milli\metre}, respectively.
%Step 2 wilcoxon
To further determine whether the differences were significant across the whole range of sampling pattern, we ran a pairwise Wilcoxon signed-rank test, with Bonferroni correction to avoid type 1 error, on each data subset, and thus determine which pair of pattern sampling rates were significantly different.
As in the first study, we found that the pairs of sampling rates were significantly different only for sampling rate below 200 and 96 points, for modulation \SI{2}{\hertz} and \SI{10}{\hertz}, respectively.
%Step 3 Quadratic mixed model
Hence, for the same motivations as the first user study, we discarded the non-significant part of the data and ran a quadratic mixed model on the significant part of the data.
The model indicated $R^2$ values of 0.70, 0.60 and 0.61 for the pattern draw frequency \SI{2}{\hertz} and circumference \SI{100}{\milli\metre}, \SI{150}{\milli\metre} and \SI{200}{\milli\metre}, respectively.
For pattern draw frequency of \SI{10}{\hertz} the model gave $R^2$ of 0.67, 0.60 and 0.60 for the patterns with circumference \SI{100}{\milli\metre}, \SI{150}{\milli\metre} and \SI{200}{\milli\metre}, respectively. 

%Step 4 Compute maximum
An $R^2$ value greater than 0.6 is considered high.
We can therefore conclude that the quadratic model is a good fit to model our data.
Then we use the coefficients from the model to estimate the pattern sampling rate that was giving the highest perceived strength.
We found that for \SI{2}{\hertz} draw frequency, the optimal sampling rate was 18.72, 20.95 and 27.95 points for circumference \SI{100}{\milli\metre}, \SI{150}{\milli\metre} and \SI{200}{\milli\metre} respectively.
We also found a sampling rate threshold of 240.55, 241.76 and 260.29 points for circumference 100, 150 and \SI{200}{\milli\metre} respectively.
For a draw frequency of \SI{10}{\hertz}, we found an optimal sampling rate of 10.77, 15.01 and 20.30 points for circumference \SI{100}{\milli\metre}, \SI{150}{\milli\metre} and \SI{200}{\milli\metre} respectively.
As for \SI{2}{\hertz} modulation, the perceived strength of pattern at \SI{10}{\hertz} plateau when the sampling rate is greater than a given number.
Using the model parameters and the plateau values, we found that the sampling rate threshold was 92.09, 90.54 and 84.52 for circumference \SI{100}{\milli\metre}, \SI{150}{\milli\metre} and \SI{200}{\milli\metre} respectively.
The results of the data fitting are summarized in Table~\ref{tab:Quad2}.

