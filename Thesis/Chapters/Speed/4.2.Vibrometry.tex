	\section{Vibrometry Study}
	\label{sec:vib_study}
%hypothesis
In this study, we wanted to test for the existence of an optimal speed to drive spatiotemporally modulated patterns, which would ideally induce maximal displacement on a surface. 
We believe that the optimal focal point speed should be equal to the surface wave propagation speed. 
Additionally, we hypothesise that speed related effects on displacement are greater than frequency related effects.
To measure the displacement induced with spatiotemporally modulated patterns, as well as their interference with resulting surface waves, we ran a series of vibrometry measurements.

	\subsection{Measurement Set-Up} 
	\label{subsec:vib_setup}
    
    	\begin{figure}
        	\centering
            \includegraphics[width=0.45\textwidth]{Figures/LDV_set-up}
            \includegraphics[width=0.45\textwidth]{Figures/LDV_Set_up}

            \caption{The Experimental Set-up was composed of 3 main elements 1)An ultrasonic phased array, 2) A silicone slab and 3) A Laser Doppler Vibrometer}~\label{fig:MeasurementSetUp}
		\end{figure}
    
    Our measurement set-up was composed of three main elements: An ultrasonic phased array to produce spatiotemporally modulated patterns, a silicone slab on which the patterns were projected and a Laser Doppler Vibrometer to measure the displacement induced by the spatiotemporally modulated patterns (as shown on Figure \ref{fig:MeasurementSetUp}). 
    
	%UH
	The ultrasonic phased array we used was a Ultrahaptics Evaluation Kit from Ultrahaptics Ltd.\footnote{https://www.ultrahaptics.com/products/evaluation-kit/} and was composed of $16\times16$ (i.e 256) ultrasound transducers. 
    The ultrasonic phased array is producing focal points \SI{8.6}{\milli\meter} in diameters at a given position and with a given acoustic power.
    The produced output can be updated with a \SI{16}{\kilo\hertz} sampling rate. 
    
    %Skin subsititute
	The spatiotemporally modulated patterns were projected on a \SI{35}{\centi\meter}$\times$\SI{35}{\centi\meter} wide and \SI{1}{\centi\meter} thick slab, cured with commercially available silicone, Ecoflex 0010\footnote{Ecoflex 0010: https://www.smooth-on.com/products/ecoflex-00-10/}, which was used as a mechanical analogue for human skin. 
	The use of silicone rather than human subjects, provided control over the measurement condition.	
    Ecoflex 0010, was selected as an analogue for human skin due to it having a similar density (\SI{1100}{\kilogram\per\cubic\meter} for human skin, where the silicone is \SI{1030}{\kilogram\per\cubic\meter}) and similar viscoelastic material properties in both surface effects and in bulk (\cite{Kearney2015,Royston2011}).
	We acknowledge that the mechanical behaviour of Ecoflex will not be the identical to real skin, due to human skin being a much more complex structure (e.g., multiple layers and anisotropy) (\cite{Gerhardt2008}), however, it is thought that the vibrometry of silicone will provide insight into the general behaviour of viscoelastic materials when excited by focused ultrasound.
    
    %LDV & principle
	Due to the small amplitude of the vibrations, we used a laser Doppler vibrometer (abbreviated to LDV) to measure them.
	The LDV is a common tool to carry out non-contact vibration measurement (\cite{Manfredi2012}).
	Vibrometry data is obtained by firing a laser beam from the LDV towards the surface to be measured and capturing reflected incident photons using a photodetector diode also inside the LDV head. Differences between the original and reflected laser signal are analysed to find the vibration modes of the reflecting surface based on the Doppler effect.
	For this study, we used a PSV-500-Scanning-Vibrometer from Polytec\footnote{https://www.polytec.com/uk/vibrometry/products/full-field-vibrometers/psv-500-scanning-vibrometer/}.
    
    %Alignment of each 3 componnents
    The silicone was placed on an experimental bench, on top of which, the ultrasonic phased-array was maintained up-side down with a stand, parallel to the silicone and at a distance of \SI{28.5}{\centi\meter}.
    The LDV was placed at a \ang{60;;} angle and pointed towards the silicone, which was \SI{36.4}{\centi\meter} away from the LDV head.
    For each measurement scan, the LDV was measuring surfaces with a resolution of \SI{1}{\milli\meter}. 
    Each measurement point lasted \SI{256}{\milli\second}, was recorded with a sampling rate of \SI{128}{\kilo\hertz} and was repeated 6 times before being averaged.
     Each measurement was synchronised between the LDV and the Ultrasonic phased array using a trigger signal. 
     Furthermore, a \SI{50}{\milli\second} null output was preceding and following each measurement.
    Two types of measurement were conducted: line measurements (see Section \ref{subsec:vib_meas}) and square measurements (see Section \ref{subsec:vib_patterns}).
    The line measurements involved a \SI{17.5}{\centi\meter} long section of the silicone and lasted 30 minutes, while the area measurements covered an area of \SI{10}{\centi\meter}$\times$\SI{10}{\centi\meter} and lasted 105 minutes.
     Micro-reflective beads were spread on the surface of the silicone to improve laser reflection and hence measurement quality.
     
     %Pre-processing
     The raw data obtained from the LDV is composed of the velocity over time for each coordinate position on the measured surface. Firstly, due to the \ang{60;;} between the LDV and the silicone, the measurements from the LDV were in a different coordinate space relative to the silicone (see Figure \ref{fig:MeasurementSetUp}). Using a Python script with the \texttt{scipy} package, we pre-processed the data, transforming each point into the correct basis using projective geometry. Further, measurements were carried in an anechoic room and band-pass filtered to remove the ultrasonic \SI{40}{\kilo\hertz} carrier frequency and remaining noise, where the low cut-off was at \SI{50}{\hertz} and the high cut-off frequency at \SI{1}{\kilo\hertz}.
    Finally, to be able to work with displacement data, we applied a time integral on the velocity data, hence obtaining the variation of displacement over time rather than the variation of velocity over time.
    We describe how we used the displacement data, according to the information we wanted to extract, in sections \ref{subsec:vib_meas} and \ref{subsec:vib_patterns}.
    
	\subsection{Preliminary Measurement}
	\label{subsec:vib_meas}
    Our study focuses on the displacement induced by the spatiotemporally modulated patterns and their associated surface waves.
    However surface waves propagate differently on different media, hence our first step was to characterise the surface wave propagation on the silicone we were using.
    To that end, we generated a focal point at the centre of the silicone slab and measured how induced surface waves propagated away from the position stimulated.
    As the silicone is a dispersive medium, surface waves with different frequencies travel at different speeds. To measure this we modulated the focal point at known frequencies ranging from \SI{200}{\hertz} to \SI{1}{\kilo\hertz} with \SI{100}{\hertz} steps. 
    We assumed the silicone to be an homogeneous and isotropic medium, and therefore focus our measurements on a single line going from the silicone slab centre towards the edge (\SI{17.5}{\centi\meter} long in total). 
    From the measurements data, we extracted the surface wave propagation speed and the frequency response of the silicone.
    
      \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]{Figures/disp_vel}
				\caption{Measured surface wave propagation speed in the silicone slab. Speed averaged around \SI{10}{\meter\per\second}}
				\label{fig:Disp_vel}
      \end{figure}
    \subsubsection{Surface wave propagation speed}
	To extract the surface wave propagation speed across the silicone, we calculated the speed at which wavefronts of surface waves propagated along the measured direction and took the average of repeated measurements of the speed.
    As predicted, the surface wave propagation speed varied with the frequency (see Figure \ref{fig:Disp_vel}) but remains in the interval of \SI{7}{\meter\per\second} to \SI{13}{\meter\per\second}, and has for average \SI{10}{\meter\per\second}. 
     The average propagation speed is slightly greater than the one measured by Manfredi et al. on the fingertip, but the general trend is similar (\cite{Manfredi2012}).
     Therefore, we assume the difference in mechanical behaviour between the two media to be responsible for the differences observed.
     
      \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]{Figures/disp_res}
				\caption{Measured frequency response in the silicone slab. One can see the silicone resonance behaviour around \SI{400}{\hertz}.}
				\label{fig:Disp_res}
      \end{figure}
     \subsubsection{Frequency response}
     To extract the frequency response of the silicone, we analysed the maximum peak-to-peak displacement at the focal point position and repeated over the frequency range.
     We found that the peak-to-peak displacement was also varying with frequency (see Figure \ref{fig:Disp_res}) and was maximum at \SI{400}{\hertz}.
     This result suggests that the silicone slab has a resonant frequency at \SI{400}{\hertz}. 
     It is sometimes suggested that human skin also possess a resonant frequency around \SI{200}{\hertz} \cite{Manfredi2012}.
     Once again, we assume the differences in material properties to be responsible for the difference in the measured resonant frequency.
    
    Overall, we can see that the silicone measurement shows similar behaviour to the skin even though the exact values differ.
    \subsection{Spatiotemporally Modulated Patterns}    
		\label{subsec:vib_patterns}

	\begin{figure}
		\centering
    \includegraphics[width=0.6\textwidth]{Figures/1}
    \caption{Example of the measurement obtained for the root-mean-squared displacement of a circular pattern.}
		\label{fig:RMSPlot}
  \end{figure}

	After characterising the surface wave propagation speed on the silicone and the silicone frequency response, we undertook to investigate the effect of surface waves on the displacement that spatiotemporal patterns induced. 
    To that end, we generated a spatiotemporally modulated circular pattern, with its centre matching the silicone centre (equivalent to Figure \ref{fig:RMSPlot}).
    We chose a circular pattern for its numerous properties (continuous, periodic, without self-crossing points), which limits possible pattern-specific artefacts.
    We then used the LDV to measure a square area of the surface encompassing the pattern (see Figure \ref{fig:RMSPlot}).
    As defined in the introduction, knowing the pattern length (here the circle perimeters), one can go from the focal point speed to the spatiotemporal modulation frequency as follow: $\mathit{FP}_{\textrm{speed}} = F_{\textrm{STM}} \times \textrm{perimeter}$.
    To compare the different effects of $\mathit{FP}_{\textrm{speed}}$ and $F_{\textrm{STM}}$ individually, we repeated the measurement while varying the perimeter and $\mathit{FP}_{\textrm{speed}}$ each in turn.
    In our data set, we had 3 different circle perimeters of \SI{5}{\centi\meter}, \SI{10}{\centi\meter}, and \SI{20}{\centi\meter} of perimeter.
    We chose these circle sizes as they could fit the user's palm that is \SI{7.5}{\centi\meter}-\SI{9.5}{\centi\meter} wide on average (\cite{Komandur2009}). 
    We picked 8 speeds around the measured average surface wave propagation speed and 4 additional speeds that match to 4 frequencies around the measured resonant frequency. 
    Yet, for certain perimeter lengths, some speed values overlapped, making for somewhere between 9 and 12 distinct speeds measured per perimeter. 
    In total 32 area measurements were taken. 
    For each measurement, we computed the root-mean-square value for peak-to-peak displacement and extracted the average value along the measured circular path (see Figure \ref{fig:RMSPlot}).
    
    \subsection{Results}
  \begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{Figures/plotDisplaSpeedOnly2.eps}
		\caption{Average root-mean-square of displacement as function of speed for circular patterns with different perimeters.}
		\label{fig:vib_result}
	\end{figure}
		\label{subsec:vib_res}
     In Figure \ref{fig:vib_result}, we plotted the measured average root-mean-square values of peak-to-peak displacement induced by focused ultrasound on circular patterns with different perimeters, for which spatiotemporal modulation is run at different speeds.
    These results show that the quantity of displacement varies with the focal point speed but remains similar across circle perimeters.
    Moreover, the displacement is maximum for speed between \SI{8} and \SI{10}{\meter\per\second}, which corresponds to the average of the surface wave propagation speed measured previously.
    Therefore, the results seem to support our hypothesis about a constructive interference between spatiotemporally modulated patterns and the wave surfaces they produced, when the focal point speed matches the speed of the surface waves propagation.
    Additionally, the results show a second maximum appearing at a focal point speed of \SI{20}{\meter\per\second}, which corresponds to twice the propagation speed of the surface waves.
    This behaviour that could be anticipated from the periodic property of the studied pattern is reminiscent of the kind of behaviour governed by ``harmonics'' often found in acoustics.
    Finally, the data does not show any evidence of a resonating mode, which should appear at 20, 40 and 80 \SI{}{\meter\per\second} for the perimeters 5, 10 and 20 \SI{}{\centi\meter}, respectively.
        
The conclusion of the current vibrometry study was finally that varying the spatiotemporal modulation speed has a large effect on the indentation of the silicone along circular patterns.
Because silicone Ecoflex-0010 possesses numerous similarities with human skin, it is likely that equivalent amplification phenomenon could be observed on human skin, but it is difficult to predict to what extent.
However, repeating the above measurement on human skin will not inform us about the consequences on the haptics of such spatiotemporally modulated patterns as they will be influenced by perceptual effects beyond simple displacement.
To investigate the perceptual implications and especially the patterns perceived strength, we decided to run a user study with similar spatiotemporally modulated patterns.
We hypothesise that there will be an effect of speed on the haptic stimulus perceived strength, although the nature of the interaction with haptics and whether it is detectable is not immediately clear.
	

