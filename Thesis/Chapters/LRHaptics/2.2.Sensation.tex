\section{Tactile Sensation}
\label{sec:sensation}
	Touch is capable of extracting a wide range of information.
	Understanding what features the sense of touch actually processes tactile information, will provide insights on designing appropriate mid-air tactile patterns.
	A tactile stimulus, or tactile sensation, arise from the skin deforming when reaching a given surface.
	Upon contact, the skin deforms differently, depending on the surface properties.
	This deformation then stimulates low threshold mechanoreceptors embedded under the skin, which encode the tactile information (i.e. deformation) into electrical activity (i.e. spikes) that travel on nerve afferents in direction to the central neural system.
	
	In this section, we discuss how skin deformation can be predicted, and how low threshold mechanoreceptors encode tactile information.

	\subsection{Skin Mechanics}
	\label{subsec:skin}
	When touching a surface, the skin deforms due to the load applied at the contact location.
	However, the skin being a complex viscoelastic medium, made of several layers, predicting how the skin deforms in a given scenario is a difficult task.
	Researchers have long sought a relation between surface physical properties and skin deformation. 

	To approach the problem, researchers undertook first, to characterise the skin mechanical properties, and then to develop equations or models predicting skin deformation in scenarios more and more complex.
	
	%The first is to use the law of continuum mechanics to model and predict analytically the skin deformation according to the load applied to its surface.
	%The other approach is to implement numerical model such as Finite Element Model (FEM).
	%In the following parts, I'll discuss briefly the skin properties to explain better the difficulty of this problem.
	%Then, I'll presente the current progress in those two approaches.

		\subsubsection{Skin properties}
		Before trying to predict skin deformation in a given scenario, it is important to understand the skin properties.
		
		%layered skin
		First, the skin is composed of two different layers.
		The first layer the epidermis, is about \SI{0.104}{\milli\meter} thick at the fingertips, while the second layer the dermis, is about \SI{1.157}{\milli\meter} thick at the fingertips (\cite{Wagner2008})
		It is important to note, that the skin is bounded to the bone, often represented as a third rigid layer.
		The epidermis and the dermis are viscoelastic, but posses different viscoelastic properties.

		%main idea: viscoelastic --> why does it matter
		Overall the skin act as a viscoelastic medium.
		Wiertlweski determined that the skin presents a dominant elastic behavior for vibratory stimuli up to 100Hz but that above this frequency, the viscous part dominates (\cite{Wiertlewski2012}). 
		Due to its viscosity the skin takes time to deform and therefore the contact area with a surface grows over time, even long after the contact has been made.
		Such observations have been measured by \citeauthor{Dzidek2016}, who also discuss the influence of such phenomenon on friction perception (\cite{Dzidek2016}).
		Similarly, skin viscous properties also affect skin relaxation time (\cite{Wang2007}).
		Table \ref{tab:skin_properties} presents some estimates of skin mechanical properties for the fingertip.
		However, even though the skin properties have been measured repeatedly, results range widely due to variance between individuals. 
		Such variance has been explained according to different factor such as age, sex and occupation.  

		%assumption
		Although the skin is often assumed homogeneous and isotropic to simplify computations, the presence of many organs, tendons, fat and so on, makes the skin a medium far from homogeneous.
		Additionally, it has been shown that the skin is not isotropic, and therefore deforms differently according to the direction the load is applied. 
		We refer the reader to \citet{Derler2012} for more details on skin tribology.


		
		\begin{table}
			\caption{Skin viscoelastic properties at the fingertip}
			\centering
			\scalebox{0.6}{
			\begin{tabular}{l|c}
				\textbf{Skin properties} & \textbf{Averange-Range} \\
				\midrule
				Stiffness \cite{Wiertlewski2012}								& \SI{0.6}{\kilo\newton\per\meter} to \SI{2}{\kilo\newton\per\meter} \\
				Elasticity (Young modulus) \cite{dandekar2003}	& \SI{0.18}{\mega\pascal} (epidermis) - \SI{0.018}{\mega\pascal} (dermis)\\
				Viscosity	\cite{Wiertlewski2012}								& 0.75 - \SI{2.38}{\newton\second\per\meter}\\
				Poisson's ratio \cite{Wu2004}										& 0.4\\
				Friction coefficient \cite{zhang1999}						& 0.46\\
			\end{tabular}
		}
			\label{tab:skin_properties}
		\end{table}

		%continuum mechanics --> analytical model
		\subsubsection{Continuum Mechanics and numerical analysis}
		%everything local
		%start with elastic only --> Hookes
		To estimate the skin deformation according to the force applied, one can use Hooke's law.

		\begin{equation}
		 \sigma = E\epsilon
		 \label{eq:hooke}
		\end{equation}
		where $\sigma$ is the static force, or stress, applied to the surface, $E$ the medium Young's modulus and $\epsilon$ the resulting deformation, or strain produced.

		%extend in space thank to Hertz
		However, in addition to deform at the location where the load is applied, skin deformation extends beyond this location.
	  Hertzian theory of contact captures how skin deforms beyond the contact area.

		Using both Hooke's law and Hertzian theory, as well as skin properties, one approach to predict skin deformation is to use the law of continuum mechanics and solve them analytically.
		\citet{Sripati2006} used this approach and applied it to the whole fingertip.
		Authors could predict deformation induced by a given static load pattern.
		However, such approach cannot be extended to dynamic load, due to the skin viscosity part becoming predominant, especially above \SI{100}{\hertz} (\cite{Wiertlewski2012}).
		The literature is abundant in model characterising viscoelastic behaviour (e.g. maxwell, kelvin Voigt, Standard linear solid model).
		However, such model are complex to solve across a whole surface, especially when model needs to account for phenomena such as surface wave propagating on the skin surface (\cite{Manfredi2012, Shao2016a}).
		This statement pushes researchers to move from analytical solution to numerical analysis approaches, such as Finite Element Model (FEM) and Finite Boundary Element Model (FBEM).
		For instance, \cite{Wu2004} have applied FEM to fingertip skin deformation.
		Authors model describe the fingertip geometry (i.e. shape) as well as its 3-layers composition (i.e. epidermis, dermis and bones) and their relative mechanical properties.
		The model is then able to predict deformation under different scenarios.

	\subsection{Mechanotransduction}
	\label{subsec:mr}
		%Use document in sussex/ressources/mechanorecptor model
		It is worth noting that skin deformation does not always evoke a tactile perception. 
		For perception to occur, the sensation (i.e. skin deformation) needs to reach the low threshold mechanoreceptors embedded in one's skin.
		There are different kinds of mechanoreceptors, which all have different properties such as density, acuity, and response characteristics.
		These mechanoreceptors properties are presented in further detail in this section.

		%Talk about mechnaoreceptor response, density, and accuracy (temporal and spatial)
		\subsubsection{Low Threshold Mechanoreceptors}

		\begin{figure}
			\centering
			\includegraphics[width=0.8\textwidth]{./Figures/finger_LTMR}
			\caption{Mechanoreceptors are localised in the dermis, under the epidermis}
			\label{fig:fingerMR}
		\end{figure}
		
		\begin{table}[b]
			\centering
			\scalebox{0.6}{
			\begin{tabular}{l|c|c|c|c}
			\textbf{Cell} & \textbf{Channel} & \textbf{Density} & \textbf{Receptive field} & \textbf{Sensitivity}\\
				\midrule
				Meissner	& RA		& \SI{140}{\per\square\centi\metre} &  \SI{12.6}{\milli\metre} & Vibrations 5-\SI{50}{\hertz} and spatial deformation \\	
				Merkel	& SAI		& \SI{70}{\per\square\centi\metre} &  \SI{11}{\milli\metre} & Constant pressure, Vibrations lower than \SI{5}{\hertz} and Spatial deformation \\	
				Pacinian	& PC		& \SI{20}{\per\square\centi\metre} &  \SI{101}{\milli\metre} & vibration 50-\SI{500}{\hertz} and  spatial deformation \\	
				Rufinni	& SAII		& \SI{20}{\per\square\centi\metre} &  \SI{59}{\milli\metre} & Constnat pressure and lateral stretch \\	

			\end{tabular}
			}
			\caption{Mechanoreceptors Properties at the fingertip}
			\label{tab:MR_properties}
		\end{table}


		%from tacdis
		As depicted in Figure \ref{fig:fingerMR}, there are four kinds of low threshold mechanoreceptors in the glabrous skin, the hairless skin covering human palms and soles.
		Because each mechanoreceptors kind encodes the deformation information, or strain,  differently, mechanoreceptors are often associated to a tactile channel describing the mechanoreceptor sensitivity and their corresponding signal on the nerve afferents. 
		There are two slowly adaptive channels, referred as SA1 and SA2, which depict the behaviour of the Merkel cells and the Ruffini endings, respectively. 
		There are two rapidly adaptive channels, one depicting the Meissner cells behavior, which is referred as RA (of alternatively FA for fast adaptive) and a second one depicting the Pacinian Corpuscle behavior, which is referred as PC.

		The repetition of the same spatiotemporal strain pattern, will trigger the same nerve activity on each of the mechanoreceptors nerve afferent (\cite{Hayward2014}). 
		However, each mechanoreceptor encodes the spatiotemporal strain pattern differently (\cite{Johansson1983}). 
		The RA channel (or NPI) encodes vibrations from 5 to \SI{50}{\hertz} and is mostly sensitive to the rate of change in the spatial deformation. 
		The PC channel (or P) encodes vibrations from 50 to \SI{500}{\hertz}.
		The SA1 channel (or NPII) is sensitive mostly to constant pressure and vibrations below \SI{5}{\hertz}. 
		The SA2 channel responds also to constant force, in addition to responding to skin stretch. 

		\begin{figure}
			\centering
			\includegraphics[width=0.6\textwidth]{./Figures/frequency_sensitivity_curve}
			\caption{Channel threshold as a function of frequency (\cite{Gescheider2002}) }
			\label{fig:frequency_sensitivity_curve}
		\end{figure}


		In addition to be sensitive to different frequency ranges, mechanoreceptors have also a different threshold of perception according to the frequency of the stimulation.
		Indeed, according to the frequency of the input stimulus, oscillation will need to occur with a greater or lower amplitude to be perceived.
		This phenomenon can be summarised in the frequency sensitivity curved represented in figure \ref{fig:frequency_sensitivity_curve} (\cite{Gescheider2004}).
		For instance oscillation around \SI{200}{\hertz} requires lower amplitude than oscillation at \SI{10}{\hertz}, to be perceived.
		In the case of the PC channel, this sensitivity will further depend on the area of the contactor (\cite{Verrillo1963}) and stimulation duration.
		These properties of the PC channel is referred as spatial and temporal summation, respectively (\cite{Gescheider2002}).
		Spatial summation means that at amplitude equivalent, oscillation over a greater contact surface are perceived stronger than those over smaller contact surface.
		Similarly, temporal summation means that at amplitude equivalent, oscillation shorter in duration are perceived weaker that those longer in duration.

		Mechanoreceptors not only react differently to strain, but also vary in distribution and receptive fields sizes. 
		Meissner and Merkel cells are the most numerous with 140 and 70 units per squared centimeters, respectively and have the smallest receptive field with \SI{12.6}{\square\milli\meter} and \SI{11}{\square\milli\meter}, respectively. 
		The Pacinian corpuscle and Ruffini endings are least numerous with 20 units per squared centimeter each and have bigger receptive field with \SI{101}{\square\milli\meter} and \SI{59}{\square\milli\meter} respectively \cite{Johansson1978, Johansson1979a}.
		These values, summarised in table \ref{tab:MR_properties}, are relative to the fingertips and are different on other body parts. 
		Hence, spatial acuity and threshold of perception might be found lower at other body location.


	
		\subsubsection{Mechanotransduction}

		%Start from freeman and johnson: mechanotransduction as electrical circuit
		The process mechanoreceptors undergo to transform mechanical stimuli into electrical spikes is referred as mechanotransduction.
		In the same way that researchers attemted to model skin deformation, researchers tried to model mechanotransduction.
		The first attempts started in the 80s and models were drawing analogy with electrical circuit (\cite{Freeman1982a}).
		The idea behind such models was to represent a mechanoreceptor as a system converting deformation into electrical potential.
		Once the potential reaches a given value (i.e threshold), a spike is emitted and the potential is reset to its resting value.
		However, mechanoreceptors are more complex and later electrical-inspired models had to include various phenomenon such as \emph{refractory state}, \emph{hyper-excitability period}, \emph{absolute threshold}, \emph{entertainment threshold}, and so on, to capture the complete mechanoreceptor behaviour.
		For further details on these phenomena, we refer the reader to \cite{Freeman1982}, we just wanted to stress here that instead of adding ad hoc modifications to electrical-inspired models, a new paradigm was required.

		%introduction of neuron model integrate-and-fire
		This new paradigm appeared recently with \cite{Dong2013a}, who proposed to separate mechanical transduction and spike emission in two parts. 
		On one side, instantaneous displacement, as well as displacement velocity and acceleration is added through a weighted sum and then run through a saturation filter. 
		The resulting signal represents the \emph{potential}, which is then use as input for an Integrate-and-Fire neuron model \cite{Mihalas2009}.
		The neuron model is accounting for all mechanoreceptors behaviours describe previously.
		After training their model based on microneurography measurements, authors, were able to predict accurately spike emission.
		Future iteration of the model from the same research group were then developed.
		To this day, the most complete model is from Saal et al. \cite{Saal2017}. 
		This model capture mechanotransduction on the whole hand as well as three kinds of mechanoreceptors, both for static and dynamic input patterns.

		
		Electrical spike emitted by the mechanoreceptors then travel on afferent nerves up to the central nervous system, where tactile information is processed.
		Little is known about how the tactile information is exactly treated once it arrives in the primary sensory cortex.
		However, the resulting interpreted information, or perception, has been studied thanks to psychophysical techniques.
		The next part covers tactile perception in further details.
