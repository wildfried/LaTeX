\section{Tactile Perception}
\label{sec:perception}

As described at the beginning of the chapter, tactile perception encompasses many physical quantities, such as roughness or hardness.
Those different physical quantities can be seen as the different dimensions of touch.
Beyond detecting a physical quantity, the sense of touch can discriminate between different amplitude of a same quantity, as well as recognising a given amplitude.
This different level of complexity in processing tactile information is what we referred as \emph{levels of perception} and are discuss in the following part, along side describing the main dimension relative to tactile perception.

	
	\subsection{Levels of Perception}
	\label{subsec:levels}
		Each dimension of touch can be categorised with different metrics relative to different levels of perception.
		These different levels of perception extend from the simple detection of a tactile sensation, to the judgement that one makes about the perceived sensation.

		%what level, how it is measured
		The first level of perception is detection.
		This is the simplest of all levels and distinguishes between a sensation being strong enough to be perceived, from one too weak to be perceived.
		Detection is often describe as absolute threshold.
		For instance, to perceive a vibrotactile stimulus, the vibration amplitude needs to be greater than a given value, referred to as threshold of perception.
		If the vibration amplitude exceed that threshold, the vibration will be perceived.
		If the vibration amplitude is lower than this threshold the vibration will not be perceived at all.
		As discuss previously, perception threshold can depend on other factors. 
		In our example, vibration amplitude threshold depends on vibration frequency. 
		Threshold of perception depends on the physical quantity perceived and there are several methods to measure absolute threshold (\cite{Macmillan2005}).

		The second level of perception is discrimination, or in other words one's ability to perceive differences in magnitude within a same dimension.
		Discriminative abilities are often referred as Just-Noticeable-Difference (JND).
		Touch, like other senses, is mainly logarithmic, hence the variation in magnitude required to be perceived, increases with stimulus amplitude.
		Therefore, JND is often express as Weber fraction and represents the amount by which the magnitude needs to increase to be noticeable.
		For instance, for vibrotactile stimulus, at low frequency, one can perceive small changes in frequency, while at higher frequency, the changes in frequency needs to be greater to be perceivable.

		Absolute Threshold and JND for the dimension of touch can be found in table \ref{tab:dim_jnd} based on results extracted from \cite{Jones2013}.
		
		The third level of perception is related to identification.
		Even though one can discriminate between various stimuli of the same nature but of different magnitudes, one's ability to recognise a specific stimulus among a given set is a more complex task.
		This ability is often measured as information transfer and expressed in bits of information.
		While for audition information transfer is around 2 or 3 bits of information according to the dimension consider, touch is often limited to 2 bits of information. 
		This means, that for instance, one can recognise only up to 4 vibrotactile stimuli if only the frequency was varied.
		Information transfer can increase using more dimension, but the exact increase depend on the dimension chosen and no clear relationship is defined.


		Finally, the last level of perception is relative to judgement.
		In other words, it is the qualitative and subjective value one attributes to the dimension perceived.
		The main judgement considered is valence, which referred to one ability to attribute a positive or a negative value to a given stimuli.
		Other judgement of different nature can also be made (e.g. arousal).

		
	\subsection{Dimension of Touch}
	\label{subsec:dimTouch}

	\begin{figure}
		\centering
		\includegraphics[width=0.9\textwidth]{./Figures/finger_dim}
		\caption{Touch encompass several dimensions: (a) Roughness, normal force and vibration induced. (b) Hardness, contact area and force amplitude increasing with the displacement. (c) Friction, tangential force opposite to the finger movement.}
		\label{fig:finger_dim}
	\end{figure}



	Tactile perception and the mechanoreceptors associated can perceive three main dimensions, namely, Roughness, Hardness and Friction.
	Temperature, pain and other quantities rely on different receptors than the one current technology in mid-air haptic displays can stimulate (see chapter \ref{ch:display}). 
	Therefore, these dimension of touch are out of the scope of this thesis.
	

		%fill next categories using tacdis paper! to update to get rid of the numbers and references to display!!
	\subsubsection{Roughness}
			Texture roughness is the perception of the irregularities or asperities that composed a surface.
			In some engineering field such as surface finish, texture roughness is usually described in terms of height profiles (e.g. \cite{ISO1997}). 
			When one's finger contact a surface, the skin deforms across the contact area.
			However, due to the irregularities and asperities in the surface, the skin does not deform evenly.
			
			Our somatosensory system developed mechanism to decode these deformations and interpret it as texture roughness.
			It is currently admitted that the perception of texture roughness is dual (\cite{Weber2013}).

			On one hand the somatosensory system uses spatial information to identify the coarse element of texture roughness (of the order of the millimeters).
			On the other hand it uses temporal information to identify the vibration induced by finer element of texture roughness (order of micrometers) (\cite{klatzky2002a,BensmaIa2003}).
			The coarse part of texture roughness, also refer as macro-texture, is perceived through the spatial variation in skin deformation. 
			However, this mechanism is limited as the space between consecutive asperities decrease. 
			This limit is measured through 2-Point-Discrimination task and is around 1-\SI{2}{\milli\meter} on the finger tip, and greater in other body locations \cite{Lederman2009}.

			To perceive finer part of texture roughness, also refer as micro-texture, tactile perception relies on the temporal variation of skin deformation, across the whole skin surface.
			As shown in \cite{Manfredi2014b}, exploring a rough surface with a finger induces vibration patterns that are correlated with the texture explored. 
			Relying on this mechanism, one can perceived gratings whose amplitude are as small as \SI{10}{\nano\meter} (\cite{Skedung2013}).


	\subsubsection{Hardness}
			By pressing down on a surface with one’s finger, one is able to determine whether an object is rigid or soft. 
			This object property is referred as texture hardness. 
			In the case of moving part (i.e. buttons), one might use the words stiffness and compliance instead. 
			Hardness is often represented using the so-called Young modulus, in Newton per square meter.

			The majority of the hardness information is encoded through the ratio force over displacement. 
			When pressing against a surface, the force will increase as the finger pushes down on the surface. 
			Many applications apply this phenomenon and implement different force over displacement curves to stimulate push-buttons with different hardness (\cite{Kildal2010,Kim2013b,Doerrer2002}).
			Recent studies also point towards a relationship between contact surface growth rate and texture hardness discrimination (\cite{Bicchi2000}). 
			In this study, authors used Hertzian equations to model contact area between a finger with an elasticity of 0.25MPa and different elastic surface whose young modulus vary from 0.125MPa to 0.500MPa at increments of 0.075MPa. 
			At \SI{2}{\newton} the contact area were distinct as \SI{1.2}{\square\milli\meter} for the two extreme elasticity values and as close as \SI{0.14}{\square\milli\meter} for the two largest elasticity values. 
			In term of spatial resolution, it is the difference between square stimuli with side difference of \SI{1.09}{\milli\meter} and \SI{0.3}{\milli\meter}, respectively. 
			Finally, Kildal shows that delivering a vibratory stimulus to the user fingers when he is pressing against a rigid surface could infer the illusion of hardness, instead of rigidity and that tuning the vibratory stimuli allow adjusting the hardness perception (\cite{Kildal2010, Kildal2012}).


		\subsubsection{Friction}
			While moving ones finger across a surface, one experiences a resistance to its movement. 
			This is referred as texture friction. 
			In solid mechanics, frictional forces are tangential to the surface and are defined as the product of the normal force against the surface and a so-called coefficient of friction, which depends on the two solids material properties. 
			However, since the finger is viscoelastic, the relationship between normal and lateral load is more complex. 
			Nonetheless, the friction coefficient between skin and a range of different materials have been measured to be in average 0.46 (\cite{zhang1999}).



			\begin{table}
				\centering
				\begin{tabular}{l|c|c}
					\textbf{Dimension} & \textbf{Threshold} & \textbf{JND}\\
					\midrule
					Roughness & \SI{0.06}{\micro\metre} & 5-12\%\\
					Hardness &  & 15-22\%\\
					Friction &  & 10-27\%\\
				\end{tabular}
				\caption{Threshold and Just-Noticeable-Difference for the 3 tactile dimension \cite{Jones2013}}
				\label{tab:dim_jnd}
			\end{table}









