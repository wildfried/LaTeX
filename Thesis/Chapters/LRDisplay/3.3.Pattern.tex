\section{Mid-air Tactile Pattern}
\label{sec:pattern}

Conveying haptic stimuli in mid-air represented a challenge that has now been overcome.
There exists now a variety of mid-air haptic display able to produce a wide range of stimulus, using various modulation technique.
Among them, ultrasonic phased arrays took advantage of their relatively high spatial resolution, high update rate and large interaction zone, to become the prevailing technology.
Using various modulation technique ultrasonic phased arrays can convey either temporal information at a localised and well-defined position, or spatiotemporal information through continuous haptic animation.
While the first kind of stimulus can be designed according to the knowledge on vibrotactile display, the second kind is relative new and understudy, both in the case of contact and contact-less display.
In the section, we will discuss a more formal and detailed definition of spatiotemporal haptic patterns.
This will be connected to a review of the related work on mid-air haptic rendering, hence highlighting the already known relations between mid-air haptic parameter space and perceptual space.

	\subsection{Definition}
	\label{subsec:pattern}

	Before defining mid-air tactile patterns, it is interesting to look upon the simplest tactile feedback: a mid-air tactile point.
	We refer as mid-air tactile point, a localised vibration produced in mid-air using either Amplitude Modulation (AM) or Lateral Modulation (LM).
	As explained in the previous section, mid-air tactile points are similar to vibrotactile stimuli.
	The perception of mid-air tactile point varies as parameters such as modulation frequency or modulation waveform change.

	Using mid-air tactile point as a building block one can produce more complex feedback such as mid-air distributed tactile pattern.
	Here mid-air distributed pattern refers to a collection of mid-air tactile points, with same or distinct modulation signal, where each of the points have a different position in the interaction space, as if distributed on a grid.
	In contact haptics, such distributed pattern are commonly produced with array of vibrotactile actuators (\cite{Schneider2015}) or pin-arrays (\cite{Sripati2006, Wang2006}). 

	Drawing analogy with vision, we can say that the approach is very similar to visual displays.
	The same way mid-air tactile points are building blocks for mid-air distributed pattern, a pixel is like the building block of visual frames.

	With such analogy in mind, mid-air tactile pattern can be defined as the haptic counter part of visual frames.
	Pushing the analogy further, one can define the resolution of a mid-air tactile pattern has the number of mid-air tactile points per unit area.
	Additionally, within a mid-air tactile pattern, each mid-air tactile points will have its parameters set according to the overall tactile information that the pattern want to convey.
	But the same way visual displays dynamically update the pixels colours, the mid-air tactile point parameters will have to be updated rapidly.
	In visual display, this issue has been overcome using raster scan. 
	In mid-air haptics, it has been hypothesised that Hilbert-curves could be an appropriate tool to render mid-air haptic pattern (\cite{Frier2016}).
	Indeed, Hilbert curves being space-filling curves, there is one Hilbert curve passing through all the mid-air tactile points positions.
	The Hilbert curve can also be chosen to match the mid-air tactile pattern resolution and be sampled to match the mid-air tactile points position.
	As in a raster scan where a beam moves rapidly through each pixels position, a mid-air tactile point could move through each position using spatiotemporal modulation.
	The intensity of the mid-air tactile point at a given sample position could be representative of the tactile information one wants to convey.
	The advantage to use space-filling curve is that the 2D surface can be unwrapped into a 1D signal (the order in which the sample position are explored).
	Once unwrapped in 1D, the texture signal can be easily process to tune the texture information to another desired sensation using signal processing strategies (\cite{Romano2010}).
	Therefore, the tactile information one wants to convey can also be resampled to match the mid-air haptic surface resolution.

	Pushing the analogy with visual displays one notch further, can help us to refine the definition of a mid-air tactile pattern.
	In graphics, visual elements of a given frame are usually handle as distinct object.
	Each object is further define into a mesh (i.e. the object geometry) and a texture (i.e. the object appearance).
	Similarly, with mid-air tactile pattern, we can define the pattern shape (i.e. the pattern geometry) and the pattern sensation (i.e. how the pattern feels).
	In graphics, the distinction between mesh and texture has numerous advantages.
	Using the same approach for mid-air tactile pattern, we could leverage the same advantages.
	Indeed, if mid-air haptic patterns are divided into two components, each component can be developed separately.
	It also means, that if one was to develop a new sensation, one could apply this sensation to all the existing shapes, rather than re-developing shapes oneself.
	Similarly, if one finds a better way to render a mid-air pattern shape, one does not need to re-develop all the sensation previously developed.
	One can just use the existing sensations with his new shape rendering algorithm.
	In term of combinatory, separating shape and sensation has the advantage to increase the set of mid-air haptic patterns available.
	Indeed, if one develops $n$ shapes and $m$ sensations, the set of available mid-air haptic patterns is $n \times m$ pattern, while having developed only $n + m$ components.
	This is particularly advantageous, if each component developed were made available to mid-air haptic designers.
	Indeed, designers will then only need to pick up the shape and the sensation to customise the mid-air haptic patterns as they desired, and would have access to a larger set of patterns.
	Finally, we would like to point out that such approach is similar to the one taken by haptic devices with force feedback.
	While user motions can be constraint through controllable torque and therefore convey the idea of shape, the feedback can be further refined with texture signals \cite{Culbertson2014}.

	%transition
	Using an analogy with visual display, we were able to define mid-air tactile pattern as the combination of two components, a shape and a sensation.
	We also determine that studying both components separately present some advantages.
	Therefore, we propose to split the field of mid-air tactile pattern rendering into two area of research, namely shape rendering and sensation rendering.
	The next two sections present the state of the art of both mid-air tactile shape and mid-air tactile sensation, respectively.

	\subsection{Mid-air tactile sensation}
	\label{subsec:sensation}
	We define in the previous section mid-air tactile sensation as the tactile feeling evoked by a given pattern.
	Unlike in chapter \ref{ch:touch}, we will use the colloquial sense of sensation as referring to both tactile sensation and tactile perception, independently of the stage of the information processing.

	Even though the terminology may differ between studies, mid-air tactile sensation has been investigated prior to our work.
	Actually, an interesting property of mid-air haptics is the ability to convey a wide range of tactile sensation to the user. 
	%pure tone
	Firstly, \citet{Obrist2013} showed that using pure tone signals and varying their frequency could greatly change the user perception and so according to different aspects.
	For instance, while a \SI{16}{\hertz} tone will be perceived as soft and pulsing, a \SI{250}{\hertz} tone will be perceived as dense and flowing.
	Interestingly, the study also report that \SI{250}{\hertz} tones were perceived stronger and bigger than \SI{16}{\hertz} tones.
	%complex signal
	In a later study \citet{Monnai2014} used percussion sounds with frequency components higher than \SI{100}{\hertz} to provide mid-air haptic feedback to the user during given interactions. 
	Interestingly, the authors reported that according to the percussion sounds chose, users perceived the mid-air haptic stimulus in a complete different manner, from stiff and light sensation to air flow burst. 
	%discriminative and affective
	While those study focused on rather abstract sensation, \cite{Freeman2017} show that mid-air haptic stimuli could also convey the feeling of realistic textures.
	Using a tessellation algorithm authors were able to render haptic surfaces with a given texture, such as gratings and grids of pyramids in mid-air.
	However, to our knowledge, other dimensions of tactile perception such as softness and friction have not been investigated in a mid-air context.


	Nonetheless, research on mid-air tactile sensation, is not limited to discriminative touch.
	\cite{Obrist2015} also demonstrate that mid-air haptic could be used to convey affective touch.
	In their study, authors show that varying parameters such as motion speed, frequency content and location stimulation could affect the emotional content conveyed through ultrasonic mid-air haptic.
	Interestingly, authors use of ultrasonic mid-air haptics unveil new insight in emotion perception that were not observed with traditional contact tactile display.
	This last point, highlight the potential of mid-air tactile display for future research.
	
	%transition
	Nonetheless, all the above studies show that a mid-air haptic stimulus temporal and spatial components, as well as their frequency equivalents, can affect the sensation conveyed with ultrasonic mid-air haptics. 
	However, in front of the plethora of parameters involved, further research are required to extend our knowledge of mid-air haptic perception and improve the efficiency of the rendering methods.

	\subsection{Mid-air tactile shape}
		\label{subsec:shape}
	We define mid-air tactile shape, the ability to convey geometric information relative to the pattern, independently of the overall pattern sensation.
	As shape is the component on which sensation will be mapped, pattern shape can be seen as the primitive of mid-air tactile patterns.
	As for sensation, rendering a shape using mid-air tactile display has been investigated prior to our work, and comes with numerous challenges that had to be overcome one after the other.


	Initial investigations on mid-air tactile patterns were focusing on freeing the mid-air tactile point from its single position and move it spatially. 
	For instance \citet{Wilson2014ApparentMotion} looked at the perception of mid-air tactile stimulus moving in a linear direction and across different distances \cite{Wilson2014ApparentMotion}.
	Authors highlighted the importance of temporal parameters according to the desired travel distances.
	These first mid-air tactile patterns were more similar unistroke patterns rather than whole shape.

	%long/korres --> multi point
	Later on, it was shown that ultrasonic mid-air displays could produce multi-points synchronously and therefore convey tactile pattern with tactile points spatially distributed in the interaction zone (\cite{Carter2013Ultrahaptics}).
	Leveraging this new ability, Long et al. demonstrate how multi-point feedback can be applied to render 3D volumetric shapes in mid-air (\cite{Long2014b}). 
	Here, the desired shapes were sampled using several tactile points and modulating the amplitude of each point.
	Furthermore, as shape perception is based on contour following, as well as vertices and edges perception, authors focussed on rendering the shape contour rather than its inside (\cite{Lederman1987, Plaisier2009}).
	Using a similar approach Korres et al. investigated the effect of varying rendering parameters for circular tactile patterns (\cite{korres2017}). 
	Therefore, most previous works concerned with tactile patterns mainly focused on the stimulation duration and the stimulus onset asynchrony. 

	However, as discussed in the previous section, using a multi-point approach to render pattern's shapes present an inherent issue.
	Indeed, the available acoustic power is limited, and producing multi-point means that this acoustic power is spread across these points.
	%STM
	To go around this issue, Kappus and Long introduced a new approach dubbed spatial temporal modulation (\cite{Kappus2018}).
	Authors show that they could trace shape's contour using solely one focal point moving rapidly and repeatedly over the desired path.

	%parameters for STM
	While the literature informs us about touch spatial \cite{Skedung2013} and temporal resolution \cite{Loomis1981} for two consecutive points separated in space and time, it is unsure how these values apply for a moving stimulus.
	Additionally, STM introduces many extra parameters such as draw speed, draw frequency, sampling rate and so on, for which the effect have not been studied.
	The work presented in these studies aim to tackle this wide parameter space and shine light on the most relevant ones.

	\vspace{5mm}

%transition to evaluation!
This section discussed how mid-air tactile patterns could be decomposed into sensation and shape, as well as the advantages the approach have.
Interestingly related work found in the literature already explore both sensation and shape rendering to some extent.
However, since mid-air tactile patterns rely on a plethora of parameters which all influence on the end perception, there is still much work required to fully understand their effect. 
The end goal of such research will be the establishment of a haptic pattern library where designers could choose the shape and sensation they desired and therefore customised their mid-air haptic patterns to the interaction they are designing.
Such library would particularly be relevant to HCI research such as for media studies \cite{Ablart2017} or art studies \cite{azh2016}.
But before building such library, the impact of mid-air tactile patterns parameters on the end perception needs to be further investigated and evaluated.
The next section will discuss what metrics could be used to evaluate and validate a given mid-air haptic pattern rendering method.
