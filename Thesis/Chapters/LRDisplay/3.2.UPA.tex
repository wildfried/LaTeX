\section{Ultrasonic Phased Array}
\label{sec:UPA}
Ultrasonic Phased Array (UPA) are composed of a collection of ultrasonic transducers. 
Even though they can come in different size and layout (\cite{Price2018}), UPAs all work on the same principle.
UPAs use phase shift to shape the acoustic field above the device accordingly to the application.
While the application could be levitating small polystyrene beads or creating parametric audio (\cite{Iodice2018, Gozel2019}), UPAs are mostly used to convey tactile feedback in mid-air.
This section will cover the creation of a single focal point whose ultrasonic pressure is great enough to deflect the skin and induce tactile sensation.
Finally, the different modulation techniques employed to convey specific tactile stimuli will be detailed.

	\begin{figure}
		\centering
		\includegraphics[width=0.9\textwidth]{Figures/focal_point}
		\caption{Mid-air Haptic Displays can focus acoustic in a so called focal point: a) 2D view, b) 3D view (\cite{Price2018})}
		\label{fig:focal_point}
	\end{figure}

		\subsection{Focal Point}
		\label{subsec:fp}
		We refer to focal point, a point or region in the acoustic field where the acoustic pressure is maximised.
		To generate a focal point there are different approaches. 

		%time of flight 
		The simplest approach is to consider the distance between the focal point desired position and the transducers.
		As the speed of sound is constant (\SI{343}{\meter\per\second} in air at NTP), one can predict how long the ultrasound wave takes to go from the transducer position to the focal point position.
		This value can be referred as the \emph{time of flight}.
		After computing the time of flight for each transducer, one can work back the delays needed between each transducer, so that each corresponding ultrasonic wave arrives at the desired position at the same time.
		These delays can be considered as phase shift between each ultrasonic wave. 

		%Long approach
		When applied to multipoint feedback, the above methods can provide different solutions. 
		Some solutions are better than other as they maximise the constructive interference at the desired location, as well as maximising the destructive interference in the remaining acoustic field.
		Such solution insures the saliency of the pattern.
		To find this optimal solution, \citet{Long2014} reformulate the problem as an eigen value problem.
		Authors show that solving the eigen value problem is similar as maximising constructive interferences at the desired location, while minimising constructive interferences everywhere else. 
		
		%Inoue approach
		The two approaches we just described both assume a free-field interaction.
		These approaches do not take into account ultrasound wave reflection on the user hand.
		However, as soon as the user moves his hand in the interaction zone, perturbations occur due to ultrasonic wave reflection at the hand surface.
		These resulting reflection can alter the acoustic field unpredictably.
		To avoid this, \citet{Inoue2016} developed an adaptive method which includes the finger geometry as part of the problem (\cite{Inoue2016}).
		To solve this new problem formulation, authors leverage the computational power of finite element modeling and therefore find the solution providing the highest amplitude despite hand interferences.

		%description of the stimulus
		Independently of the approach used, a focal point is generally described as a position and an amplitude, which can either remain constant or be varied through time according to the scenario (see sections \ref{subsec:AM}-\ref{subsec:STM}).
		The amplitude of a focal point produced with a given ultrasonic phased array can be estimated as long as the position and the ultrasonic transducers properties are known.

		%total acoustic pressure
		For instance, in a free field, using Huygens-Fresnel principle and the principle of superposition of waves, one can estimate the interference of $N$ ultrasound waves, with wavenumber $k$, and determine the total pressure produced P, to be equal to
		\begin{equation}
			P = \sum\limits_{n=1}^N \frac{P_0}{r_n} H(\theta_n) e^{ikr_n}
			\label{eq:total_pressure}
		\end{equation}

	Where $P_0$ is the pressure produced by a transducer $n$ positioned at a distance $r_n$ and angle $\theta_n$ from the focal point and which possesses a directivity function $H(\theta)$. 

		%total 
		However, the focal point created is not punctual in the strict mathematical term and actually posses a width, that can also be determined.
		It has been been shown that the diameter $w$ of the focal point is related to the ultrasound wavelength $\lambda$ and estimated empirically as 
		\begin{equation}
			w = 1.22 \frac{\lambda}{sin\theta}, \quad  sin\theta = \frac{D/2}{\sqrt{R^2 + (D/2)^2}}
			\label{eq:size_fp}
		\end{equation}

		Where $R$ and $D$ represent the focal length and the aperture diameter respectively (\cite{Ito2016}).
		%size
		Using these equations, the focal point size is assumed to be \SI{8.5}{\milli\meter} wide, with traditional ultrasound phased array using 40kHz ultrasonic transducers, while being \SI{5}{\centi\metre} wide when using 70kHz ultrasonic transducers.
		
		At this point, it is worth mentioning that for rectilinear arrays, the focal point has been shown to be rather elongated in the axis going away from the array surface (\cite{Price2018}).
		A representation of the acoustic field when a focal point is produced \SI{20}{\milli\meter} above the ultrasonic phase array, can be seen on figure \ref{fig:focal_point}

		The acoustic pressure at the focal point leads to acoustic radiation pressure, a non-linear acoustic phenomenon that results in a non-zero time averaged force on a surface.
		In the case of a focal point produced with an array composed of 256 \SI{40}{\kilo\hertz} transducers, acoustic radiation pressure can reach \SI{16}{\milli\pascal}.
		Such pressure, once applied to the skin, is enough to slightly deform the skin, hence evoking tactile sensation.
		Further, details on how to derive acoustic radiation pressure will be described in section \ref{subsec:ARP}. 

		\vspace{5mm}

		%transition
		When the pressure amplitude is kept constant our sense of touch is sensitive only to the onset and the offset of such pressure (see section \ref{subsec:mr}).
		However, our sense is sensitive to dynamic pressure when the amplitude variation are within a rate of change of 0-\SI{500}{\hertz}.
		Based on these statements, and because of the aim to produce long duration tactile feedback, researchers developed various modulations techniques. 
		The next section covers these techniques and their differences.
		
		\subsection{Amplitude Modulation}
		\label{subsec:AM}

	\begin{figure}
		\centering
		\includegraphics[width=0.6\textwidth]{Figures/AM}
		\caption{Amplitude Modulation: To create a pattern (here a circle), one needs multipoint whose amplitudes vary across time}
		\label{fig:am}
	\end{figure}
		%What it is
		In most of the human computer interactions, feedback are aimed to last few hundreds of millisecond.
		In order to create a tactile feedback that was perceived to be this long, researchers developed feedback with dynamic feature.
		One way to create such dynamic features is to continuously vary the focal point amplitude over time between a minimum and maximum amplitude.
		This approach is referred as Amplitude Modulation (AM) and makes mainly use of sinusoidal signals to modulate the focal point amplitude.
		
		%how it is used.
		As stated previously, the sense of touch is sensitive to vibration in the range 0-\SI{500}{\hertz}, however the sense of touch sensitivity through that range varies (\cite{Gescheider2002}). 
		Various studies report a threshold of perception being lower at frequency 200-\SI{250}{\hertz}, and so independently of the location at which the study was carried or the size of the contactor used.
		Hence, to produce the strongest perceived sensation, amplitude modulation is commonly used around these frequencies. 
		Nonetheless, it has been shown that in addition to affect the strength of the sensation, frequency was responsible for the overall perception of the stimuli (\cite{Obrist2013}).
		Indeed, in the study, participants reported different qualitative properties whether the focal point amplitude was modulated at low frequency (i.e. \SI{16}{\hertz}) or high frequency (i.e. \SI{250}{\hertz}).
		\cite{Monnai2014} also reported using non-sinusoidal signals to convey different sensation.
		
		%\todo{positive: like vibrotactile}
		It is worth highlighting here that such method produces similar results than traditional vibrotactile devices.
		Indeed, when the focal amplitude increases and decreases, the skin deformation also increases and decreases.
		It is similar to a linear actuator indenting the skin.
		As the indenter goes into the skin, the skin deformation increases, and as then the indenter retracts, the skin deformation decreases.
		The analogy is important here, because it means that the results from the community on the perception of vibrotactile signals can be adapted to mid-air haptics and amplitude modulation.
		An argument towards this is the fact that results reported in \citet{Obrist2013} and \citet{Monnai2014} are coherent with results presented in \cite{Maclean2003} where both frequency and signal waveform affect the user perception of the tactile stimulus.

		%\todo{negative: individual pressure drops as number increase}
		Amplitude Modulation assumes the focal point position to remain constant through the stimulus, while this is effective for local stimulation, the approach finds its limit when rendering distributed patterns.
		Indeed, to create spatially distributed patterns, one needs to produce several amplitude modulated points (\cite{Long2014}).
		Since a single ultrasonic phased array can only produce so much pressure, the absolute amplitude created at each point decrease drastically as the number of simultaneous points increase.
		While \citet{Long2014} proposed a time multiplexing approach to limit this phenomenon, the number of focal point that can be simultaneously produced and perceived remains limited.
		In turn this limit the distributed pattern complexity that can be created.
		This limit lead researcher to explore new modulation techniques.

		\subsection{Lateral Modulation}
		\label{subsec:LM}

	\begin{figure}
		\centering
		\includegraphics[width=0.6\textwidth]{Figures/LM}
		\caption{Lateral Modulation: To create a pattern (here a circle), one needs multipoint whose positions oscillate across time, while the amplitude stays at its maximum}
		\label{fig:lm}
	\end{figure}
		%what it is
		The strength of mid-air tactile stimulus is limited by the pressure it can produce. 
		Even though one could increase the number of element in an ultrasonic phase array to increase the pressure the array can produce, one can also make better used of the available pressure.
		In other words modulate the pressure in a way that low threshold mechanoreceptors are more sensitive to.
		For instance, we saw that low threshold mechanoreceptors are more sensitive to vibration around \SI{200}{\hertz} and that therefore AM signals should be tuned to that same frequency.
		Similarly, \cite{Biggs2002} showed that low threshold mechanoreceptors were more sensitive to lateral forces than normal ones.
		Leveraging this fact, \citet{Takahashi2018} had the idea to modulate the focal point position rather than its amplitude (\cite{Takahashi2018}).
		Hence, by moving the point position from side to side around a target position on the user's palm, authors succeed to produce focal point that are perceivably stronger than focal point created using amplitude modulation.
		This approach to move laterally a focal point is referred as Lateral Modulation (LM).
		%how it is used
		In their study, authors investigated two parameters, namely the rate at which the oscillation occurred and the amplitude of those oscillations.
		Authors found that stimuli were perceived the strongest for oscillations between 50 and \SI{200}{\hertz} and at least \SI{5}{\milli\meter} amplitude.
		%pros and cons
		Lateral Modulation produces stimuli noticeably stronger than the ones produced with Amplitude Modulation.
		However, when it comes to spatially distributed patterns, the same issue exist, the pattern complexity is limited by the number of tactile points that can be rendered at once.
		Additionally, one can easily imagine the impact of Lateral Modulation on the spatial resolution.
		Therefore, another method still need to be found.

		\subsection{Spatiotemporal Modulation}
		\label{subsec:STM}
	\begin{figure}
		\centering
		\includegraphics[width=0.6\textwidth]{Figures/STM}
		\caption{Spatiotemporal Modulation: To create a pattern (here a circle), one needs a single points moving repeatedly over the circle path, while the amplitude stays at its maximum}
		\label{fig:stm}
	\end{figure}
		%what it is
		When alternating between two pictures rapidly and repeatedly, one sees only one single and full picture.
		This phenomenon is relying on the persistence of vision and inspired visual toys such as the thaumatrope.
		Recently \cite{Kappus2018} had the idea to adapt this phenomenon to mid-air haptics by moving rapidly and repeatedly a focal point on a given path.
		For instance, a focal point moving fast on a circular trajectory is perceived as a whole circle rather than a moving point.
		This effect is related to the temporal resolving capacity of one's sense of touch, which has been measured to be around 2 to \SI{40}{\milli\second} (\cite{Loomis1981}).
		This approach is referred as Spatiotemporal Modulation (STM) and is therefore mainly used to draw spatially distributed patterns on one's palm.
		The advantages of STM over the other two modulation techniques is that it uses only one focal point per pattern instead of many.
		Hence, all the pressure that can be generated by ultrasonic phased arrays is therefore concentrated in that single focal point.
		This results in perceivably stronger patterns, especially as the pattern size and complexity increase.
		%sign posting!
		Currently the designers' approach is to repeat the desired pattern at rate equivalent to the other modulation type (i.e. around \SI{200}{\hertz}).
		Furthermore, a pattern repeated at \SI{200}{\hertz}, means that a single iteration takes \SI{5}{\milli\second} which agrees with touch temporal resolving capacity. 
		However, the technique is not only recent to the field of mid-air haptics, but also to the haptic community in general.
		Therefore, it is unknown whether this approach is the best or parameters different from the repetition rate should be optimised instead.

		\vspace{5mm}

		This Thesis being interested with rendering spatially distributed patterns, the choice of using spatiotemporal modulation has been chosen.
		However, ultrasonic phase arrays being a recent technology, its parameter space, and its associated perceptual space, still need to be investigated.
		In the next section, we will define what we call a mid-air tactile pattern and how we imagine its parameter space.
		Based on this definition we will present existing studies that relate to mid-air tactile patterns.
