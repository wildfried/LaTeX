\section{Contributions}
\label{sec:contribution}
	The main goal of the work presented in this thesis was to investigate how to convey stronger spatially distributed mid-air tactile patterns.
	To do so, we leveraged the rendering capabilities of a recent modulation technique, namely Spatiotemporal Modulation.
	While most researchers focused their effort on engineering and algorithmic challenges, which the development of mid-air haptic displays using spatiotemporal modulation, little attention has been paid to the relationship between spatiotemporal parameters space and its associated perceptual space.
	In that sense our research is the first step towards  a better understanding of the relationship between STM parameter space and STM perceptual space.
	Our research results are mainly valuable to the mid-air haptic experience designers, who can reuse our contributions within their work.
	However, our results also provide insights into the perception of spatiotemporal tactile pattern, which are beneficial to the broader haptic community.
	The current section summarise our contributions to both communities.

	\subsection{Mid-air Haptic Applications}
	\label{subsec:app}
		Until now, mid-air haptic experience designers were targeting temporal information at a given location, to convey tactile information to the users.
		Additionally, designers could further tune the temporal information to change the corresponding sensation to their desire.
		However, to convey the perception of shape, designers were constrained to align multiple tactile points along the desired shape.
		This approach is consequently limited by the number of tactile point that can be produced simultaneously without decreasing their relative strength under the threshold of perception.
		Hence, only simple and small mid-air tactile patterns could be produced.

		With Spatiotemporal modulation, this constraint is no longer present, and mid-air haptic experience designers can start producing bigger and more complex shapes.
		Although, the drawback of this method, is that since the modulation technique is recent, guidelines relative to STM usage are absent from the literature.
		Designers, therefore, have to tweak the various variables from spatiotemporal parameter space without knowing their impact on the perceptual space.
		Through successive studies, we attempted to overcome this issue and provide the designers with insightful guidelines to render Spatiotemporal patterns.

		Recall that rendering spatiotemporal patterns involves moving a mid-air tactile point rapidly and repeatedly.
		Hence, one can define two parameters, namely the draw speed and the draw frequency, which represent the speed at which the haptic point moves, and the rate at which the pattern is repeated, respectively.
		Furthermore, the draw speed, $S$ is proportional to the draw frequency $F$, with the pattern length $L$ as coefficient of proportionality. 
		
		$$S = F \times L$$§

		Designers might have intuitively been tempted to tune the drawing frequency, as it is usually done with Amplitude Modulation (see section \ref{subsec:AM}).
		Hence, setting the drawing frequency around \SI{200}{\hertz}, which is the frequency perceived the strongest.
		However, we show in chapter \ref{ch:speed}, that on the contrary, the approach should be to tune the draw speed, for two reasons. 
		\begin{itemize}
			\item Optimising draw speed maximises pattern perceived strength
			\item Fixing draw speed causes perceived strength to be invariant to scaling 
		\end{itemize}
		These results were demonstrated through both a vibrometry study (see section \ref{sec:vib_study}) and a user study (see section \ref{sec:speed_us}).
		
		%sampling rate or update rate?
		Furthermore, we showed in chapter \ref{ch:sampling} that sampling rate was another important variable from spatiotemporal parameter space.
		Recall, that sampling rate designates the number of sample positions the mid-air haptic point goes through to draw one pattern iteration.
		Until now, and especially for vibrotactile stimuli, sampling rate was always related to pattern quality and therefore maximised whenever possible.
		However, our results highlight that for draw frequency under \SI{10}{\hertz}, changing the sampling rate could have an important impact on pattern perceived strength.
		Yet, sampling rate effect on the pattern is twofold.
		First, increasing or decreasing the sampling rate, decrease and increase the distance between two sample positions. 
		Second, increasing or decreasing the sampling rate, decrease and increase the stimulation duration at each sample position.
		Before our study, it was unknown which of the two quantities was the most relevant.
		Our user studies revealed that optimising the distance between two sample position could maximise pattern perceived strength for draw frequency lower than \SI{10}{\hertz}.
		However, the optimal distance was varying with the draw frequency.
		The contributions of these studies are therefore twofold.
		\begin{itemize}
			\item Optimising distance between two sample position maximises pattern perceived strength, for a draw frequency lower than \SI{10}{\hertz}
			\item Fixing distance between two sample position causes perceived strength to be invariant to scaling. 
		\end{itemize}

		Additionally, our results extended the lower limit of the perceptual space from draw frequency at \SI{10}{\hertz} to draw frequency at \SI{2}{\hertz}.
		Thanks to Weber's Law, we know that the sense of touch, as every other senses, is logarithmic. 
		Therefore, this new range represents an increase of 50\% of the range of draw frequency one can perceived until now.

		All these discoveries on spatiotemporal modulation parameter space is beneficial to mid-air haptic experience designers.
		Indeed, thanks to our contributions, designers can use our guidelines to improves mid-air tactile pattern perceived strength, as well as ensuring constant perceived strength through scaling.
		One could easily imagine these guidelines be implementing on a mid-air haptic authoring tools, so designers can remain oblivious to the intricacies of mid-air haptic perception, and focus on designing compelling  user experience.
		Our results also provide insights into the perceptual mechanism related to spatiotemporal modulation.
		These contributions are of interest to the larger haptic perception community and are discussed in the next section.
		

	\subsection{Haptic Perception}
	\label{subsec:community}
		While our main goal was to provide mid-air haptic experience designers with a set of guidelines in order to design better and stronger mid-air haptic pattern, our results also provide an interesting set of contributions for the haptic perception community, both in the biomechanics and psychophysics fields.

		%new hardware!
		First, it is worth noting the unique nature of ultrasonic phased array compare to other contact and non-contact tactile displays.
		Usually tactile displays stimulate the skin in specific and predetermined locations.
		State of the art tactile displays include either a dense amount of actuators but cover a small area (\cite{Sripati2006, Wang2006}) or cover a large area but include few actuators (\cite{Schneider2015}).
		Ultrasonic phased arrays, especially using spatiotemporal modulation, are the first occurrence where a display can stimulate a large area such as the palm with a high spatial resolution.
		This particularity makes ultrasonic phased arrays a unique opportunity to study the perception of rapid and repeated stimulation of the skin. 
		This is confirmed with the results of our studies, which make new contribution the haptic perception community and are discussed in this section.
		These contributions are reported in this section.
		
		%Biomechanics
		To the field of biomechanics, our work provides new insights on wave propagating on the skin surface.
		Propagating waves on the skin have already been observed in the literature (\cite{Manfredi2012}).
		The fact that propagating waves travels fast and far on the skin surface lead researchers to stress their importance in texture perception (\cite{Delhaye2012}) and whole hand interactions (\cite{Shao2016a}).
		However, these studies focus on characterising the propagation of skin surface waves from a unique source.
		With our vibrometry study (see section \ref{sec:vib_study}), on the other hand, we are able to stimulate the skin on various location and observed the interference between the corresponding propagating waves.
		In particular, our vibrometry study revealed that producing a spatiotemporal pattern with a rendering speed equivalent to the propagating waves speed is maximising the displacement produced on the medium.
		Hence, we showed that mid-air tactile patterns could be rendered in such a way that they are interacting with the same propagating wave they produced, in order to optimise the displacement they induced when projecting on a viscoelastic surface.
		We later showed in a user study, that on such specific scenario, the user perceived strength was increased.
		While previous research highlight the importance of propagating waves in haptic perception, our work is the first record of propagating waves being a central component of the haptic pattern design itself.

		%psychophysics
		To the field of psychophysics, the work presented here contains valuable data set that refine further the relationship between spatiotemporal modulation parameter space and its associated perceptual space.
		Through a set of three users studies, we determine the effect of 4 parameters (i.e. draw frequency, draw speed, pattern size and pattern sampling rate) on the pattern perceived strength.
		In all three studies, we systematically use scaling task (i.e. Magnitude Estimation) to measure the effect of each parameter on the pattern perceived strength.
		The first user study (see section \ref{sec:speed_us}) showed how perceived strength vary with mid-air pattern size and draw speed.
		While the results showed that draw speed affects greatly the pattern perceived strength, they also show that smaller pattern (i.e. \SI{5}{\centi\meter} long) are perceived weaker than bigger ones (i.e. above \SI{10}{\centi\meter} long).
		In the second and third user study (see sections \ref{sec:sampling_us1} and \ref{sec:sampling_us2}), results showed that perceived strength is further affected by sampling rate, and especially the resulting distance between two consecutive sample positions.
		As in the first study, the perceived strength is significantly lower for smaller pattern than for bigger ones.
		Using the results of these studies, we can also conclude on the order of importance of each parameter.
		Hence, we show that the perceived strength depends mainly on pattern draw speed.
		However, at speed lower than \SI{2}{\meter\per\second}, the perceived strength depends mainly on pattern sampling rate.
		Finally, in both cases, studies show that pattern size plays a lower role in pattern perceived strength.

		%new framework
		Overall, the work reported here open new horizons on how haptic perception mechanism processes spatiotemporal patterns.
		Until now tactile perception was mostly related to vibrotactile perception, which rely on a small parameter space mainly dominated by the signal temporal components (e.g. frequency and waveform).
		Therefore, only mechanoreceptors frequency sensitivity was considered.
		With spatiotemporal modulation, we show that this was a limitation and that now signal spatial components should be considered too, to take into account factors such as mechanoreceptor field and tactile point width.
		We discuss in chapter \ref{ch:spatial} how the assumption regarding the tactile point width and the mechanoreceptor receptive field area, could lead to misleading conclusion regarding the stimulation duration of a given mechanoreceptor during spatiotemporal modulation.
		Additionally, spatiotemporal modulation motivates researchers to consider more than the fundamental frequency composed the temporal information of a mid-air haptic stimulus.
		Accounting for these new considerations, we undertook to define a new framework of research for the specific case of spatiotemporal patterns.
		This new research framework reject the assumptions previously made for the study of vibrotactile stimuli.
		While it still needs refinement, we additionally contribute towards the development of the blocks composing this new framework of research.
		Especially, we propose a paradigm shift, where instead of observing the evolution of the tactile point in space over time, we observe the pressure applied to a given location over time.
		In other words, instead of observing the behaviour of the tactile point over time, we propose to observe the evolution of skin displacement at the skin surface over time.

		While our work present various contribution to both the mid-air haptic experience designers community and the haptic perception community, we acknowledge that our work present a set of limitations.
		The next section will discuss these limitations.


